package godau.fynn.lawdirect;

import godau.fynn.lawdirect.network.Network;
import godau.fynn.lawdirect.util.Resolver;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ResolverTest {

    @BeforeClass
    public static void setUp() {
        Network.assumeNetworkAvailable = true;
    }

    @Test
    public void resolveGesetzeImInternet() throws IOException {

        assertEquals(
                "Bürgerliches Gesetzbuch",
                Resolver.resolve("/gesetze-im-internet.de/bgb").getTitle()
        );

        assertEquals(
                "Law providers",
                Resolver.resolve("////").getTitle()
        );
    }
}
