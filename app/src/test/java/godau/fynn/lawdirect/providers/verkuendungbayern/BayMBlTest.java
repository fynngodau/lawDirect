package godau.fynn.lawdirect.providers.verkuendungbayern;

import godau.fynn.lawdirect.model.parent.PublicationContentList;
import godau.fynn.lawdirect.network.Network;
import godau.fynn.lawdirect.util.Resolver;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BayMBlTest {

    @BeforeClass
    public static void setUp() {
        Network.assumeNetworkAvailable = true;
    }

    @Test
    public void getPublicationItems() throws IOException {
        VerkuendungBayernPublication bayMBL = new BayMBl();
        List<PublicationContentList> list = bayMBL.getPublicationItems(0);

        assertEquals(15, list.size());

        assertTrue(list.get(0).getId().startsWith("202"));

    }

    @Test
    public void resolveBayMBl() throws IOException {

        assertEquals(
                Resolver.resolve("/verkuendung-bayern.de/BayMBl")
                        .getTitle(),
                "Bayerisches Ministerialblatt"
        );

        assertEquals(
                "2020 Nr. 348",
                Resolver.resolve("/verkuendung-bayern.de/BayMBl/2020-348")
                        .getTitle()
        );

    }
}