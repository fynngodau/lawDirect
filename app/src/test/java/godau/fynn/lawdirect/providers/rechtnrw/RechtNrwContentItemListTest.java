package godau.fynn.lawdirect.providers.rechtnrw;

import godau.fynn.lawdirect.model.parent.Publication;
import godau.fynn.lawdirect.model.parent.PublicationContentList;
import godau.fynn.lawdirect.network.Network;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class RechtNrwContentItemListTest {
    @BeforeClass
    public static void setUp() {
        Network.assumeNetworkAvailable = true;
    }

    @Test
    public void getPublicationItems() throws IOException {
        Publication publication = new GVNrw();
        List<PublicationContentList> list = publication.getPublicationItems(0);

        assertTrue(list.get(0).getId().matches("\\d{5}"));

    }
}