package godau.fynn.lawdirect.providers.verkuendungbayern;

import godau.fynn.lawdirect.network.Network;
import godau.fynn.lawdirect.util.Resolver;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class GVBlTest {

    @BeforeClass
    public static void setUp() {
        Network.assumeNetworkAvailable = true;
    }

    @Test
    public void resolveGVBl() throws IOException {

        assertEquals(
                Resolver.resolve("/verkuendung-bayern.de/GVBl")
                        .getTitle(),
                "Gesetz- und Verordnungsblatt"
        );

        assertEquals(
                "2020 S. 446",
                Resolver.resolve("/verkuendung-bayern.de/GVBl/2020-446-1")
                        .getTitle()
        );

    }
}
