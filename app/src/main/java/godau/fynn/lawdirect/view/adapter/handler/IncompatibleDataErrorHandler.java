package godau.fynn.lawdirect.view.adapter.handler;

import godau.fynn.lawdirect.R;

public class IncompatibleDataErrorHandler extends StaticHandler {

    @Override
    protected int getLayout() {
        return R.layout.row_incompatible_data_error;
    }

    public static class Page extends IncompatibleDataErrorHandler {

        @Override
        protected int getLayout() {
            return R.layout.page_incompatible_data_error;
        }
    }
}
