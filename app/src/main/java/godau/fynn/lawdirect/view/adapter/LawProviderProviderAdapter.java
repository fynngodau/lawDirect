package godau.fynn.lawdirect.view.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.parent.LawProvider;
import godau.fynn.lawdirect.view.adapter.holder.ListItemViewHolder;
import godau.fynn.typedrecyclerview.SimpleRecyclerViewAdapter;

import java.util.List;

public class LawProviderProviderAdapter extends SimpleRecyclerViewAdapter<LawProvider, LawProviderProviderAdapter.LawProviderViewHolder> {

    public LawProviderProviderAdapter(List<LawProvider> content) {
        super(content);
    }

    @NonNull
    @Override
    public LawProviderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LawProviderViewHolder(
                inflater.inflate(R.layout.row_law_provider, parent, false), "", (FragmentActivity) context
        );
    }

    @Override
    public void onBindViewHolder(@NonNull LawProviderViewHolder holder, LawProvider item, int position) {
        holder.bind(item);
    }

    protected static class LawProviderViewHolder extends ListItemViewHolder {

        private final ImageView faviconView;

        public LawProviderViewHolder(@NonNull View itemView, String path, FragmentActivity context) {
            super(itemView, path, context);

            faviconView = itemView.findViewById(R.id.favicon);
        }

        @Override
        public void bind(ListItem listItem) {
            super.bind(listItem);

            LawProvider provider = (LawProvider) listItem;

            faviconView.setTag(listItem);

            if (provider.getFavicon() == null) {
                // Hide view
                faviconView.setVisibility(View.GONE);
            } else {
                // Show view
                faviconView.setVisibility(View.VISIBLE);
                // Load image
                Picasso.get()
                        .load(provider.getFavicon())
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                if (faviconView.getTag() == listItem) {
                                    faviconView.setImageBitmap(bitmap);
                                }
                            }

                            @Override
                            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                faviconView.setImageDrawable(placeHolderDrawable);
                            }
                        });
            }
        }
    }
}
