package godau.fynn.lawdirect.view.adapter;

import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.content.RepealedItem;
import godau.fynn.lawdirect.util.indicator.IncompatibleDataIndicatorItem;
import godau.fynn.lawdirect.util.indicator.LoadingIndicatorItem;
import godau.fynn.lawdirect.util.indicator.NetworkErrorIndicatorItem;
import godau.fynn.lawdirect.view.adapter.handler.*;
import godau.fynn.typedrecyclerview.TypeHandler;

import java.util.List;

/**
 * Displays each element full-screen for paginated layouts.
 */
public class ContentListAdapter extends ListItemAdapter {

    private final TypeHandler<?, ? extends ListItem> repealedItem = new RepealedItemHandler(path),
            defaultHandler = new DefaultPageHandler(path);

    public ContentListAdapter(String path, List<ListItem> listItems) {
        super(path, listItems);
    }

    @Override
    public Class<? extends TypeHandler<?, ? extends ListItem>> getItemHandlerClass(ListItem item, int position) {
        if (item instanceof IncompatibleDataIndicatorItem) {
            return IncompatibleDataErrorHandler.Page.class;
        } else if (item instanceof NetworkErrorIndicatorItem) {
            return NetworkErrorHandler.Page.class;
        } else if (item instanceof LoadingIndicatorItem) {
            return LoadingIndicatorHandler.Page.class;
        } else {
            return null; // Goto getItemHandler
        }
    }

    @Override
    public TypeHandler<?, ? extends ListItem> getItemHandler(ListItem item, int position) {
        if (item instanceof RepealedItem) {
            return repealedItem;
        } else {
            return defaultHandler;
        }
    }

}
