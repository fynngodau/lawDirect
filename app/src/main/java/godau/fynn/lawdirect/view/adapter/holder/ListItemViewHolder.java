package godau.fynn.lawdirect.view.adapter.holder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.activity.fragment.FragmentFactory;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.persistence.Downloader;
import godau.fynn.lawdirect.persistence.DownloadService;

import java.io.IOException;

import static godau.fynn.lawdirect.persistence.DownloadService.EXTRA_ITEM;
import static godau.fynn.lawdirect.persistence.DownloadService.EXTRA_PATH;

public class ListItemViewHolder extends RecyclerView.ViewHolder {

    private final FragmentActivity context;

    private final TextView title;
    private final TextView content;
    protected final View card;

    private final String path;


    public ListItemViewHolder(@NonNull View itemView, String path, FragmentActivity context) {
        super(itemView);

        this.context = context;

        title = itemView.findViewById(R.id.title);
        content = itemView.findViewById(R.id.content);
        card = itemView.findViewById(R.id.card);

        this.path = path;

    }

    public void bind(final ListItem listItem) {

        if (listItem.getTitle() == null)
            title.setVisibility(View.GONE);
        else {
            title.setVisibility(View.VISIBLE);
            title.setText(listItem.getTitle());
        }

        if (listItem.getText() == null)
            content.setVisibility(View.GONE);
        else {
            content.setVisibility(View.VISIBLE);
            content.setText(listItem.getText());
        }

        if (listItem instanceof ParentItem) {
            card.setOnClickListener(v -> context.getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_right)
                    .replace(R.id.fragment, FragmentFactory.getFragment((ParentItem) listItem, path))
                    .addToBackStack(null)
                    .commit());
            card.setOnLongClickListener(v -> {
                new AlertDialog.Builder(context)
                        .setItems(R.array.download_options, (dialog, which) -> {

                            if (which == 0) {

                                ProgressDialog progressDialog = new ProgressDialog(context);
                                progressDialog.setMessage(context.getString(R.string.download_progress, listItem.getTitle()));
                                progressDialog.show();

                                new Thread(() -> {
                                    try {
                                        new Downloader.ParentOnly(path, (ParentItem) listItem, context).run();
                                    } catch (IOException e) {
                                        Toast.makeText(context, R.string.network_error, Toast.LENGTH_SHORT).show();
                                    }
                                    context.runOnUiThread(progressDialog::dismiss);
                                }).start();

                            } else {
                                Intent intent = new Intent(context, DownloadService.class);
                                intent.putExtra(EXTRA_PATH, path);
                                intent.putExtra(EXTRA_ITEM, listItem);
                                context.startForegroundService(intent);
                            }

                        })
                        .show();
                return true;
            });
        }
    }

    /**
     * Variant of <code>ListItemViewHolder</code> which must be scrolled to the top when binding
     */
    public static class Scrollable extends ListItemViewHolder {

        public Scrollable(@NonNull View itemView, String path, FragmentActivity context) {
            super(itemView, path, context);
        }

        @Override
        public void bind(ListItem listItem) {
            super.bind(listItem);

            card.scrollTo(0, 0);
        }
    }
}
