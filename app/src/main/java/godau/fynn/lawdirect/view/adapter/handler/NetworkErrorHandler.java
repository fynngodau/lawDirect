package godau.fynn.lawdirect.view.adapter.handler;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.util.indicator.NetworkErrorIndicatorItem;
import godau.fynn.lawdirect.view.adapter.holder.NetworkErrorViewHolder;
import godau.fynn.typedrecyclerview.TypeHandler;

public class NetworkErrorHandler extends TypeHandler<NetworkErrorViewHolder, NetworkErrorIndicatorItem> {

    @Override
    public NetworkErrorViewHolder createViewHolder(@NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.row_network_error, parent, false);
        return new NetworkErrorViewHolder(view);
    }

    @Override
    public void bindViewHolder(@NonNull NetworkErrorViewHolder holder, NetworkErrorIndicatorItem item, int position) {
        holder.setRetryRunnable(item.getRetryRunnable());
    }

    public static class Page extends NetworkErrorHandler {

        @Override
        public NetworkErrorViewHolder createViewHolder(@NonNull ViewGroup parent) {
            View view = inflater.inflate(R.layout.page_network_error, parent, false);
            return new NetworkErrorViewHolder(view);
        }
    }
}
