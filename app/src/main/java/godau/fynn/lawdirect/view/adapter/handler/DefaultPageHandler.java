package godau.fynn.lawdirect.view.adapter.handler;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.view.adapter.holder.ListItemViewHolder;
import godau.fynn.typedrecyclerview.TypeHandler;

public class DefaultPageHandler extends TypeHandler<ListItemViewHolder, ListItem> {

    protected final String path;

    public DefaultPageHandler(String path) {
        this.path = path;
    }

    @Override
    public ListItemViewHolder createViewHolder(@NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.page_paragraph_list, parent, false);
        return new ListItemViewHolder.Scrollable(view, path, (FragmentActivity) context);
    }

    @Override
    public void bindViewHolder(@NonNull ListItemViewHolder holder, ListItem item, int position) {
        holder.bind(item);
    }
}
