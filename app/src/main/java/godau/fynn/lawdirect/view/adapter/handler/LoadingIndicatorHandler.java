package godau.fynn.lawdirect.view.adapter.handler;

import godau.fynn.lawdirect.R;

public class LoadingIndicatorHandler extends StaticHandler {

    @Override
    protected int getLayout() {
        return R.layout.row_loading_indicator;
    }

    public static class Page extends LoadingIndicatorHandler {

        @Override
        protected int getLayout() {
            return R.layout.page_loading_indicator;
        }
    }
}
