package godau.fynn.lawdirect.view.adapter.handler;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.PreferenceManager;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.view.adapter.ListItemAdapter;
import godau.fynn.lawdirect.view.adapter.holder.ListItemViewHolder;
import godau.fynn.typedrecyclerview.TypeHandler;

public class DefaultHandler extends TypeHandler<ListItemViewHolder, ListItem> {

    private final String path;

    public DefaultHandler(String path) {
        this.path = path;
    }

    @Override
    public ListItemViewHolder createViewHolder(@NonNull ViewGroup parent) {
        @LayoutRes int layout;
        switch (PreferenceManager.getDefaultSharedPreferences(context).getString("layout_default", "row_list_item_with_arrow")) {
            default:
            case "row_list_item_with_arrow":
                layout = R.layout.row_list_item_with_arrow;
                break;
            case "row_list_item_card":
                layout = R.layout.row_list_item_card;
        }

        View view = inflater.inflate(layout, parent, false);
        return new ListItemViewHolder(view, path, (FragmentActivity) context);
    }

    @Override
    public void bindViewHolder(@NonNull ListItemViewHolder holder, ListItem item, int position) {
        holder.bind(item);
    }
}
