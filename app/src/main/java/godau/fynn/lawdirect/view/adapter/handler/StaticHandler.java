package godau.fynn.lawdirect.view.adapter.handler;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.typedrecyclerview.TypeHandler;

/**
 * Binder that only inflates a layout and does not need to do anything on bind. Thus, it
 * will always display the same layout.
 */
public abstract class StaticHandler extends TypeHandler<RecyclerView.ViewHolder, ListItem> {

    @Override
    public final RecyclerView.ViewHolder createViewHolder(@NonNull ViewGroup parent) {
        View view = inflater.inflate(getLayout(), parent, false);
        return new RecyclerView.ViewHolder(view) {
        };
    }

    @Override
    public final void bindViewHolder(@NonNull RecyclerView.ViewHolder holder, ListItem item, int position) {
    }

    protected abstract @LayoutRes int getLayout();
}
