package godau.fynn.lawdirect.view.adapter.holder;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.lawdirect.R;

public class NetworkErrorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private Runnable retryRunnable;

    public NetworkErrorViewHolder(@NonNull View itemView) {
        super(itemView);

        itemView.findViewById(R.id.retry)
                .setOnClickListener(this);
    }

    public void setRetryRunnable(Runnable retryRunnable) {
        this.retryRunnable = retryRunnable;
    }

    @Override
    public void onClick(View v) {
        retryRunnable.run();
    }
}
