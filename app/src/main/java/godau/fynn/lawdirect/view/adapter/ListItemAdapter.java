package godau.fynn.lawdirect.view.adapter;

import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.util.indicator.IncompatibleDataIndicatorItem;
import godau.fynn.lawdirect.util.indicator.LoadingIndicatorItem;
import godau.fynn.lawdirect.util.indicator.NetworkErrorIndicatorItem;
import godau.fynn.lawdirect.view.adapter.handler.DefaultHandler;
import godau.fynn.lawdirect.view.adapter.handler.LoadingIndicatorHandler;
import godau.fynn.lawdirect.view.adapter.handler.NetworkErrorHandler;
import godau.fynn.lawdirect.view.adapter.handler.IncompatibleDataErrorHandler;
import godau.fynn.typedrecyclerview.TypeHandler;
import godau.fynn.typedrecyclerview.TypedRecyclerViewAdapter;

import java.util.List;

public class ListItemAdapter extends TypedRecyclerViewAdapter<ListItem> {

    protected final String path;
    private final DefaultHandler defaultHandler;

    public ListItemAdapter(String path, List<ListItem> listItems) {
        super(listItems);
        this.path = path;
        defaultHandler = new DefaultHandler(path);
    }

    @Override
    public Class<? extends TypeHandler<?, ? extends ListItem>> getItemHandlerClass(ListItem item, int position) {
        if (item instanceof IncompatibleDataIndicatorItem) {
            return IncompatibleDataErrorHandler.class;
        } else if (item instanceof NetworkErrorIndicatorItem) {
            return NetworkErrorHandler.class;
        } else if (item instanceof LoadingIndicatorItem) {
            return LoadingIndicatorHandler.class;
        } else {
            return null; // return default handler from other method
        }
    }

    @Override
    protected TypeHandler<?, ? extends ListItem> getItemHandler(ListItem item, int position) {
        return defaultHandler;
    }

    public String getTitle(int position) {
        return content.get(position).getTitle();
    }
}
