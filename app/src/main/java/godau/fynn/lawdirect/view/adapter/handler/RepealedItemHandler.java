package godau.fynn.lawdirect.view.adapter.handler;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.view.adapter.ContentListAdapter;
import godau.fynn.lawdirect.view.adapter.holder.ListItemViewHolder;

public class RepealedItemHandler extends DefaultPageHandler {

    public RepealedItemHandler(String path) {
        super(path);
    }

    @Override
    public ListItemViewHolder createViewHolder(@NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.page_repealed, parent, false);
        return new ListItemViewHolder(view, path, (FragmentActivity) context);
    }
}
