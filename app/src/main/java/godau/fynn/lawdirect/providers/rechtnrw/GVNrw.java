package godau.fynn.lawdirect.providers.rechtnrw;

public class GVNrw extends RechtNrwPublication {

    @Override
    String getUrl() {
        return "https://recht.nrw.de/lmi/owa/BR_VBL_GV_RSS_AUFRUFEN";
    }

    @Override
    public String getTitle() {
        return "Gesetz- und Verordungsblatt NRW";
    }

    @Override
    public String getShortcode() {
        return "GV. NRW.";
    }
}
