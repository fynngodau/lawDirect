package godau.fynn.lawdirect.providers.offline;

import android.text.Spanned;
import android.util.Log;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.parent.BaseParentItem;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.persistence.FileManager;
import godau.fynn.lawdirect.persistence.ItemDao;
import godau.fynn.lawdirect.persistence.ItemDataDatabase;
import godau.fynn.lawdirect.util.indicator.IncompatibleDataIndicatorItem;

import java.io.File;
import java.io.IOException;
import java.io.InvalidClassException;
import java.util.ArrayList;
import java.util.List;

public class OfflineParentItem extends BaseParentItem {

    private final String path;
    private transient ItemDao items;
    private transient List<String> childIds;
    private transient FileManager fileManager;

    private static final int PAGE_LIMIT = 100;

    public OfflineParentItem(String path, ParentItem overwritten, ItemDao items) {
        path = path + "/" + overwritten.getId();
        if (path.equals("/")) {
            path = "";
        }
        this.path = path;

        this.items = items;
    }

    @Nullable
    @Override
    public List<? extends ListItem> getContent(int page) throws IOException {

        if (childIds == null) {
            if (items == null) {
                items = ItemDataDatabase.getItemDao(getContext());
            }

            childIds = items.getChildIds(path);
        }

        if (fileManager == null) {
            fileManager = new FileManager(getContext());
        }

        List<ListItem> listItems = new ArrayList<>();


        for (int i = page * PAGE_LIMIT; i < childIds.size() && i < (page + 1) * PAGE_LIMIT; i++) {
            String childId = childIds.get(i);
            File file = new File(getContext().getFilesDir(), childId);

            ListItem addItem;

            try {
                if (file.isDirectory()) {
                    addItem = fileManager.read(new File(file, "__THIS__"));
                } else {
                    addItem = fileManager.read(file);
                }
            } catch (ClassNotFoundException | InvalidClassException e) {
                Log.e(OfflineParentItem.class.getSimpleName(), "This item has been downloaded with an older, incompatible version.");
                addItem = new IncompatibleDataIndicatorItem(getContext());
            }

            if (addItem instanceof BaseParentItem) {
                BaseParentItem baseAddItem = (BaseParentItem) addItem;

                baseAddItem.overwriteContent(new OfflineParentItem(path, baseAddItem, items));
            }

            listItems.add(addItem);
        }

        if (listItems.size() == 0) {
            return null;
        }

        return listItems;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Nullable
    @Override
    public Spanned getText() {
        return null;
    }

    @Override
    public String getId() {
        return null;
    }
}
