package godau.fynn.lawdirect.providers.verkuendungbayern;

import android.text.Spanned;
import android.text.SpannedString;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.WebUrl;
import godau.fynn.lawdirect.model.content.ContentItem;
import godau.fynn.lawdirect.model.parent.PublicationContentList;
import godau.fynn.lawdirect.network.Network;
import godau.fynn.lawdirect.util.HtmlUnnumberedItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VerkuendungBayernContentList extends PublicationContentList implements WebUrl {

    private String fundstelle, titel, url;

    public VerkuendungBayernContentList(String fundstelle, String titel, String url) {
        this.fundstelle = fundstelle;
        this.titel = titel;
        this.url = url;
    }

    @Override
    public List<ContentItem> getContentItems(int page) throws IOException {

        if (page > 0) return null;

        List<ContentItem> list = new ArrayList<>();

        Document d = Jsoup.parse(
                Network.string(getNetwork().request(url, null, "GET"))
        );

        list.add(new HtmlUnnumberedItem(d.getElementById("documentbox").html()));

        return list;
    }

    @Override
    public String getTitle() {
        return fundstelle;
    }

    @Nullable
    @Override
    public Spanned getText() {
        return new SpannedString(titel);
    }

    @Override
    public String getId() {
        String[] urlParts = url.split("/");
        return urlParts[urlParts.length - 1];
    }

    @Override
    public String getUrl() {
        return url;
    }
}
