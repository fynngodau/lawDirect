package godau.fynn.lawdirect.providers.verkuendungbayern;

import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.WebUrl;
import godau.fynn.lawdirect.model.parent.LawProvider;
import godau.fynn.lawdirect.model.ParentItem;

import java.util.ArrayList;
import java.util.List;

public class VerkuendungBayernProvider extends LawProvider implements WebUrl {
    @Override
    public List<ParentItem> getLawItems() {
        List<ParentItem> lawItems = new ArrayList<>();
        lawItems.add(new BayMBl());
        lawItems.add(new GVBl());
        return lawItems;
    }

    @Override
    public String getTitle() {
        return "Verkündungsplattform Bayern";
    }

    @Nullable
    @Override
    public String getDataSourceName() {
        return "BAYERN.RECHT";
    }

    @Override
    public String getId() {
        return "verkuendung-bayern.de";
    }

    @Override
    public String getFavicon() {
        return "https://www.verkuendung-bayern.de//typo3conf/sites/baymbl/images/favicon.ico";
    }

    @Override
    public String getUrl() {
        return "https://www.verkuendung-bayern.de/";
    }
}
