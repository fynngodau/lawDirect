package godau.fynn.lawdirect.providers.rechtnrw;

import android.text.Html;
import android.text.Spanned;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.WebUrl;
import godau.fynn.lawdirect.model.content.ContentItem;
import godau.fynn.lawdirect.model.parent.PublicationContentList;
import godau.fynn.lawdirect.util.HtmlUnnumberedItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RechtNrwContentList extends PublicationContentList implements WebUrl {
    private final String titel, description, url;

    public RechtNrwContentList(String titel, String description, String url) {
        this.titel = titel;
        this.description = description;
        this.url = url;
    }

    @Override
    public List<ContentItem> getContentItems(int page) throws IOException {

        if (page > 0) return null;

        List<ContentItem> list = new ArrayList<>();

        Document d = Jsoup.parse(getNetwork().request(url, null, "GET"), "ISO-8859-1", url);
        Element mainElement = d.getElementById("main1");
        mainElement.getElementsByTag("h1").remove();
        mainElement.getElementsByTag("br").remove();
        mainElement.getElementsByAttributeValue("name", "NORMKOPF").remove();
        mainElement.getElementsByAttributeValue("name", "NORM").remove();
        mainElement.getElementsByTag("table").remove();
        mainElement.getElementsByClass("container").remove();

        list.add(new HtmlUnnumberedItem(mainElement.html()));

        return list;
    }

    @Override
    public String getTitle() {
        return this.titel;
    }

    @Nullable
    @Override
    public Spanned getText() {
        return Html.fromHtml(description);
    }

    @Override
    public String getId() {
        Pattern pattern = Pattern.compile("([a-z]*_id=)([0-9]*)");
        Matcher matcher = pattern.matcher(url);
        matcher.find();
        return matcher.group(2);
    }

    @Override
    public String getUrl() {
        return url;
    }
}
