package godau.fynn.lawdirect.providers.verkuendungbayern;

import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.util.Resolver;

import java.io.IOException;

public class GVBl extends VerkuendungBayernPublication {
    @Override
    public String getTitle() {
        return "Gesetz- und Verordnungsblatt";
    }

    @Override
    public String getShortcode() {
        return "GVBl";
    }

    @Override
    public String getUrl() {
        return "https://www.verkuendung-bayern.de/gvbl/";
    }

    @Nullable
    @Override
    protected ListItem getItem(String id) throws IOException {
        String[] parts = id.split("-");
        if (parts.length < 2) {
            return null;
        }
        return Resolver.findInList(
                getPublicationItems(0, getUrl() + "?volume=" + parts[0] + "&page=" + parts[1]),
                id
        );
    }
}
