package godau.fynn.lawdirect.providers.test;

import android.text.Spanned;
import android.text.SpannedString;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.Searchable;
import godau.fynn.lawdirect.model.content.ContentItem;
import godau.fynn.lawdirect.model.parent.Publication;
import godau.fynn.lawdirect.model.parent.PublicationContentList;
import godau.fynn.lawdirect.network.Network;
import godau.fynn.lawdirect.util.StringContentItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class TestPublication extends Publication {
    @Override
    public List<PublicationContentList> getPublicationItems(int page) {

        if (page > 0) return null;

        return Collections.singletonList(new TestPublicationContentList());
    }

    @Override
    public String getTitle() {
        return "Verkündungen";
    }

    @Override
    public String getShortcode() {
        return "Vk";
    }

    private static class TestPublicationContentList extends PublicationContentList implements Searchable {
        @Override
        public List<ContentItem> getContentItems(int page) throws IOException {

            List<ContentItem> contentItemList = new ArrayList<>();

            switch (page) {
                case 0:
                    contentItemList.add(new StringContentItem("0", "Vorläufige Überschrift", "Die Überschriften in dieser Verordnung sind unverzüglich zu ändern."));
                    return contentItemList;
                case 1:
                    contentItemList.add(new StringContentItem("1", "Inkraftreten", "Diese Verkündung tritt mit dem auf den Verkündungstag foldengen Tag in Kraft."));
                    return contentItemList;
                case 2:
                    String text = Network.string(getNetwork().request("https://notabug.org/fynngodau/DSBDirect/raw/master/versionCode", null, "GET"), "UTF-8");
                    contentItemList.add(new StringContentItem(text, "Version " + text, "Version code"));
                    return contentItemList;
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    text = "Line one\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nHi";
                    contentItemList.add(new StringContentItem(String.valueOf(20 + page), "§" + (20 + page), text));
                    return contentItemList;
                default:
                    return null;

            }
        }

        @Override
        public String getTitle() {
            return "Regelungen zu Überschriften";
        }

        @Nullable
        @Override
        public Spanned getText() {
            return new SpannedString("RzÜ");
        }

        @Override
        public String getId() {
            return "RzÜ";
        }

        @Override
        public List<? extends ListItem> search(String query, int page) {
            return null;
        }
    }
}
