package godau.fynn.lawdirect.providers.test;

import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.Searchable;
import godau.fynn.lawdirect.model.content.ContentItem;
import godau.fynn.lawdirect.model.hierarchy.Book;
import godau.fynn.lawdirect.model.hierarchy.HierarchisedContentItem;
import godau.fynn.lawdirect.util.StringContentItem;

import java.util.Arrays;
import java.util.List;

class TestBook extends Book implements Searchable {
    @Override
    public String getShortcode() {
        return "FYNN1";
    }

    @Override
    public List<HierarchisedContentItem> getContentItems(int page) {

        if (page > 0) return null;

        return Arrays.asList(
                new StringContentItem("1", "§1", "Die Würde des Main-Spessart-Expresses ist unbegreifbar."),
                new StringContentItem("2", "§2", "Sie zu achten und zu schützen ist Verpflichtung der DB Regio AG Bayern.")
        );
    }

    @Override
    public String getTitle() {
        return "Gesetzbuch";
    }

    @Override
    public List<? extends ListItem> search(String query, int page) {

        return Arrays.asList(
                new StringContentItem("" + page, "§" + page, "Result of query " + query),
                new StringContentItem(page + "a", "§" + page + "a", "Result of query ")
        );
    }
}
