package godau.fynn.lawdirect.providers.gesetzeiminternet;

import android.text.Html;
import android.text.Spanned;
import godau.fynn.lawdirect.model.hierarchy.NumberedItem;

public class GesetzeImInternetContentItem extends NumberedItem {
    private String html;

    public GesetzeImInternetContentItem(String number, String title, String html) {
        super(number, title);

        this.html = html;
    }

    @Override
    public Spanned getText() {
        return Html.fromHtml(html, null, new GesetzeImInternetTagHandler());
    }

    @Override
    public String getId() {
        if (number.isEmpty()) {
            return "0";
        } else {
            return number;
        }
    }
}
