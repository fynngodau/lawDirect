package godau.fynn.lawdirect.providers;

import android.content.SharedPreferences;
import android.text.Spanned;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceManager;
import godau.fynn.lawdirect.model.parent.LawProvider;
import godau.fynn.lawdirect.model.parent.SinglePageParentItem;
import godau.fynn.lawdirect.providers.offline.OfflineProvider;
import godau.fynn.lawdirect.providers.gesetzeiminternet.GesetzeImInternetProvider;
import godau.fynn.lawdirect.providers.rechtnrw.RechtNrwProvider;
import godau.fynn.lawdirect.providers.test.TestProvider;
import godau.fynn.lawdirect.providers.verkuendungbayern.VerkuendungBayernProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LawProviderProvider extends SinglePageParentItem {

    /**
     * @return All law providers in existence, no matter whether
     * enabled or not.
     */
    public List<LawProvider> getAllLawProviders() {
        return Arrays.asList(
                // Real providers
                new VerkuendungBayernProvider(),
                new RechtNrwProvider(),
                new GesetzeImInternetProvider(),

                // Dummy provider
                new TestProvider(),

                // Offline storage provider
                new OfflineProvider()
        );
    }

    /**
     * Only returns enabled law providers.
     *
     * @return All law providers that are not disabled
     * @see #getAllLawProviders()
     */
    @Nullable
    @Override
    public List<LawProvider> getContent() {

        // For testing resolvers, return all except database provider if context is null
        if (getContext() == null) {
            List<LawProvider> list = new ArrayList<>(getAllLawProviders());
            list.remove(new OfflineProvider());
            return list;
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        List<LawProvider> lawProviders = new ArrayList<>();

        for (LawProvider lawProvider : getAllLawProviders()) {
            boolean enabled = sharedPreferences.getBoolean("enable_provider_" + lawProvider.getId(), true);

            if (enabled) {
                lawProviders.add(lawProvider);
            }
        }

        return lawProviders;
    }

    @Override
    public String getTitle() {
        return "Law providers";
    }

    @Nullable
    @Override
    public Spanned getText() {
        return null;
    }

    @Override
    public String getId() {
        return "";
    }
}
