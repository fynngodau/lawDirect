package godau.fynn.lawdirect.providers.rechtnrw;

import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.WebUrl;
import godau.fynn.lawdirect.model.parent.LawProvider;
import godau.fynn.lawdirect.model.ParentItem;

import java.util.ArrayList;
import java.util.List;

public class RechtNrwProvider extends LawProvider implements WebUrl {
    @Override
    public List<ParentItem> getLawItems() {
        List<ParentItem> lawItems = new ArrayList<>();
        lawItems.add(new GVNrw());
        lawItems.add(new MBlNrw());
        return lawItems;
    }

    @Override
    public String getTitle() {
        return "Landesrecht NRW";
    }

    @Nullable
    @Override
    public String getDataSourceName() {
        return "recht.nrw.de";
    }

    @Override
    public String getId() {
        return "recht.nrw.de";
    }

    @Override
    public String getFavicon() {
        return "https://recht.nrw.de/favicon.ico";
    }

    @Override
    public String getUrl() {
        return "https://recht.nrw.de/";
    }
}
