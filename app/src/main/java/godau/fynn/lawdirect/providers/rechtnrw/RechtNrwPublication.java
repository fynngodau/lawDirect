package godau.fynn.lawdirect.providers.rechtnrw;

import godau.fynn.lawdirect.model.parent.Publication;
import godau.fynn.lawdirect.model.parent.PublicationContentList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class RechtNrwPublication extends Publication {

    private transient Elements elements;

    @Override
    public List<PublicationContentList> getPublicationItems(int page) throws IOException {
        List<PublicationContentList> publicationItems = new ArrayList<>();

        if (elements == null) {
            Document rssDoc = Jsoup.parse(
                    getNetwork().request(getUrl(), null, "GET"),
                    "ISO-8859-1", getUrl(), Parser.xmlParser()
            );

            elements = rssDoc.getElementsByTag("item");
        }

        if (page < elements.size()) {
            Element rssItem = elements.get(page);
            String link = rssItem.getElementsByTag("link").text();

            Document d = Jsoup.parse(
                    getNetwork().request(link, null, "GET"), "ISO-8859-1", link
            );
            Elements tables = d.select("table[cellspacing]");
            for (Element table : tables) {
                Elements trs = table.getElementsByTag("tr");
                trs.remove(0);
                Collections.reverse(trs);
                for (Element tr : trs) {
                    Elements tds = tr.getElementsByTag("td");
                    String seite = tds.get(4).text();
                    String datum = tds.get(1).text();
                    String description = tds.get(3).getElementsByTag("a").first().text();
                    String url = tds.get(3).getElementsByTag("a").first().attr("abs:href");
                    publicationItems.add(
                            new RechtNrwContentList("Seite " + seite + " – " + datum, description, url)
                    );
                }
            }
        } else {
            return null;
        }

        return publicationItems;
    }

    abstract String getUrl();
}
