package godau.fynn.lawdirect.providers.gesetzeiminternet;

import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.model.Searchable;
import godau.fynn.lawdirect.model.parent.LawProvider;
import godau.fynn.lawdirect.network.Network;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class GesetzeImInternetProvider extends LawProvider implements Searchable {

    @Override
    public List<ParentItem> getLawItems() throws IOException {
        List<ParentItem> publicationItems = new ArrayList<>();

        Network network = getNetwork();

        Document d = Jsoup.parse(
                network.request("https://www.gesetze-im-internet.de/gii-toc.xml", null, "GET"),
                "UTF-8", "https://www.gesetze-im-internet.de/gii-toc.xml", Parser.xmlParser()
        );
        Elements elements = d.getElementsByTag("item");

        for (Element element : elements) {
            String title = element.getElementsByTag("title").text();
            String link = element.getElementsByTag("link").text();


            publicationItems.add(new GesetzeImInternetBook(title, link));

        }

        return publicationItems;
    }

    @Override
    public String getTitle() {
        return "Deutsches Bundesrecht";
    }

    @Override
    public List<? extends ListItem> search(String query, int page) throws IOException {

        String queryUrl = "https://www.gesetze-im-internet.de/cgi-bin/htsearch?config=Titel_bmjhome2005&method=and&words=" +
                URLEncoder.encode(query, "ISO-8859-1") +
                "&page=" +
                (page + 1);

        org.jsoup.nodes.Document d = Jsoup.parse(
                getNetwork().request(queryUrl, null, "GET"), "ISO-8859-1", "https://www.gesetze-im-internet.de/cgi-bin/htsearch"
        );

        Element div = d.getElementById("paddingLR12");
        Elements dls = div.getElementsByTag("dl");

        List<ListItem> searchResults = new ArrayList<>();

        for (Element dl : dls) {
            Elements a = dl.getElementsByTag("a");
            String url = a.attr("abs:href");
            String title = a.text().replace(" - nichtamtliches Inhaltsverzeichnis", "");

            searchResults.add(new GesetzeImInternetBook(title, url + "xml.zip"));
        }

        if (searchResults.size() == 0) {
            return null;
        }

        return searchResults;
    }

    @Nullable
    @Override
    public String getDataSourceName() {
        return "gesetze-im-internet.de";
    }

    @Override
    public String getId() {
        return "gesetze-im-internet.de";
    }

    @Nullable
    @Override
    public String getFavicon() {
        return "https://www.gesetze-im-internet.de/favicon.ico";
    }
}
