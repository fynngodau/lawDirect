package godau.fynn.lawdirect.providers.rechtnrw;

public class MBlNrw extends RechtNrwPublication {

    @Override
    String getUrl() {
        return "https://recht.nrw.de/lmi/owa/BR_VBL_MBL_RSS_AUFRUFEN";
    }

    @Override
    public String getTitle() {
        return "Ministerialblatt NRW";
    }

    @Override
    public String getShortcode() {
        return "MBl. NRW.";
    }
}
