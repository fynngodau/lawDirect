package godau.fynn.lawdirect.providers.test;

import android.text.Spanned;
import android.text.SpannedString;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.model.content.ContentItem;
import godau.fynn.lawdirect.model.parent.ContentList;
import godau.fynn.lawdirect.model.parent.BaseParentItem;
import godau.fynn.lawdirect.model.parent.PublicationContentList;
import godau.fynn.lawdirect.util.indicator.IncompatibleDataIndicatorItem;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class ErrorProvider extends BaseParentItem {

    @Override
    public List<ParentItem> getContent(int page) throws IOException {
        if (page == 0) return Arrays.asList(
                new PublicationContentList() {
                    @Override
                    public List<ContentItem> getContentItems(int page) throws IOException {
                        throw new IOException();
                    }

                    @Override
                    public String getTitle() {
                        return "Error content";
                    }

                    @Override
                    public Spanned getText() {
                        return new SpannedString("Throws network error when loading");
                    }

                    @Override
                    public String getId() {
                        return "networkerror";
                    }
                },
                new BaseParentItem() {
                    @Override
                    public List<ListItem> getContent(int page) {

                        if (page > 6) return null;

                        if (page == 2) {
                            return Collections.singletonList(new IncompatibleContentList());
                        }

                        return Collections.singletonList(
                                new IncompatibleDataIndicatorItem(getContext())
                        );
                    }

                    @Override
                    public String getTitle() {
                        return "Incompatible provider";
                    }

                    @Override
                    public Spanned getText() {
                        return new SpannedString("Adds incompatible indicator items");
                    }

                    @Override
                    public String getId() {
                        return "incompatible";
                    }

                    class IncompatibleContentList extends ContentList {

                        public IncompatibleContentList() {
                            overwriteContent(new BaseParentItem() {
                                @Nullable
                                @Override
                                protected List<? extends ListItem> getContent(int page) {
                                    if (page < 5) {
                                        return Collections.singletonList(new IncompatibleDataIndicatorItem(getContext()));
                                    }
                                    return null;
                                }

                                @Override
                                public String getTitle() {
                                    return null;
                                }

                                @Nullable
                                @Override
                                public Spanned getText() {
                                    return null;
                                }

                                @Override
                                public String getId() {
                                    return null;
                                }
                            });
                        }

                        @Override
                        public List<ContentItem> getContentItems(int page) {
                            return null;
                        }

                        @Override
                        public String getTitle() {
                            return "Incompatible tabs";
                        }

                        @Nullable
                        @Override
                        public Spanned getText() {
                            return new SpannedString("Display incompatible items in a tabs layout");
                        }

                        @Override
                        public String getId() {
                            return "tabs";
                        }
                    }
                }
        );

        if (page == 1) throw new IOException();

        return null;
    }

    public Spanned getText() {
        return new SpannedString("Test different error UIs");
    }

    @Override
    public String getId() {
        return "error";
    }

    @Override
    public String getTitle() {
        return "Error provider";
    }
}
