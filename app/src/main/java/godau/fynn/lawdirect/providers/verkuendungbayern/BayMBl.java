package godau.fynn.lawdirect.providers.verkuendungbayern;

import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.util.Resolver;

import java.io.IOException;
import java.net.URLEncoder;

public class BayMBl extends VerkuendungBayernPublication {

    @Override
    public String getTitle() {
        return "Bayerisches Ministerialblatt";
    }

    @Override
    public String getShortcode() {
        return "BayMBl";
    }

    @Override
    public String getUrl() {
        return "https://www.verkuendung-bayern.de/baymbl/";
    }

    @Nullable
    @Override
    protected ListItem getItem(String id) throws IOException {
        return Resolver.findInList(
                getPublicationItems(0, getUrl() + "?reference=" + URLEncoder.encode(id.replace("-", " Nr. "), "UTF-8")),
                id
        );
    }
}
