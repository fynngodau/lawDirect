package godau.fynn.lawdirect.providers.gesetzeiminternet;

import android.text.Spanned;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.content.RepealedItem;
import godau.fynn.lawdirect.model.hierarchy.Book;
import godau.fynn.lawdirect.model.hierarchy.Heading;
import godau.fynn.lawdirect.model.hierarchy.HierarchisedContentItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipFile;

public class GesetzeImInternetBook extends Book {
    private final String title;
    private final String url;

    public GesetzeImInternetBook(String title, String url) {
        this.title = title;
        this.url = url;
    }

    @Override
    public List<HierarchisedContentItem> getContentItems(int page) throws IOException {
        if (page > 0) {
            return null;
        }

        String prefix = url.split("/")[3];
        while (prefix.length() < 3) {
            prefix += "-";
        }

        File file = File.createTempFile(prefix, ".zip", getContext().getCacheDir());

        FileOutputStream fileOutputStream = new FileOutputStream(file);

        InputStream inputStream = getNetwork().request(url.replace("http:", "https:"), null, "GET");
        ByteArrayOutputStream fileBytes = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            fileBytes.write(buffer, 0, length);
        }
        inputStream.close();

        fileOutputStream.write(fileBytes.toByteArray());
        fileBytes.close();
        fileOutputStream.close();

        ZipFile zipFile = new ZipFile(file);

        InputStream fileStream = zipFile.getInputStream(zipFile.entries().nextElement());
        Document d = Jsoup.parse(fileStream, "UTF-8", "", Parser.xmlParser());

        List<HierarchisedContentItem> contentItems = new ArrayList<>();

        Elements paragraphElements = d.getElementsByTag("norm");

        for (Element paragraphElement : paragraphElements) {

            Element segmentation = paragraphElement.getElementsByTag("gliederungseinheit").first();
            if (segmentation != null) {

                String segmentationUnitLabel = segmentation.getElementsByTag("gliederungsbez").text();
                String segmentationUnitTitle = segmentation.getElementsByTag("gliederungstitel").text();
                String segmentationUnitNumber = segmentation.getElementsByTag("gliederungskennzahl").text();

                int segNumLen = segmentationUnitNumber.length();
                int[] position = new int[segNumLen / 3];
                for (int i = 0; i < (segNumLen / 3); i++) {
                    position[i] = Integer.parseInt(segmentationUnitNumber.substring(i * 3, Math.min(segNumLen, i * 3 + 3)));
                }

                contentItems.add(new Heading(position, segmentationUnitLabel, segmentationUnitTitle));

            } else {

                String contentHtml = paragraphElement.getElementsByTag("textdaten").first()
                        .getElementsByTag("text")
                        .html();

                String paragraphNumber = paragraphElement.getElementsByTag("enbez").text();
                String paragraphTitle = paragraphElement.getElementsByTag("titel").text();

                // Find repealed items
                if (
                        (paragraphNumber != null && paragraphNumber.matches("\\(XXXX\\)|\\(weggefallen\\)"))
                                || (paragraphTitle != null && paragraphTitle.matches("\\(XXXX\\)|\\(weggefallen\\)"))
                                || contentHtml.matches("\\s*(<\\w+>\\s*)+(-|\\(weggefallen\\))(\\s*</\\w+>)+\\s*")
                ) {
                    contentItems.add(new RepealedItem(
                            paragraphNumber == null ? paragraphNumber : paragraphNumber.replaceAll("\\(XXXX\\)\\s?|\\s?\\(weggefallen\\)", ""),
                            paragraphTitle == null ? paragraphTitle : paragraphTitle.replaceAll("\\(XXXX\\)\\s?|\\s?\\(weggefallen\\)", ""),
                            paragraphNumber, getContext())
                    );
                } else {
                    contentItems.add(new GesetzeImInternetContentItem(paragraphNumber, paragraphTitle, contentHtml));
                }
            }
        }

        file.delete();
        zipFile.close();

        return contentItems;
    }

    @Override
    public String getId() {
        return url.split("/")[3];
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Nullable
    @Override
    public Spanned getText() {
        return null;
    }

    @Override
    public String getShortcode() {
        return null;
    }
}
