package godau.fynn.lawdirect.providers.verkuendungbayern;

import godau.fynn.lawdirect.model.WebUrl;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.Searchable;
import godau.fynn.lawdirect.model.parent.Publication;
import godau.fynn.lawdirect.model.parent.PublicationContentList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public abstract class VerkuendungBayernPublication extends Publication implements Searchable, WebUrl {
    @Override
    public List<PublicationContentList> getPublicationItems(int page) throws IOException {
        return getPublicationItems(page, getUrl() + "?");
    }

    /**
     * @param url Must allow adding more parameters with '&'
     */
    protected List<PublicationContentList> getPublicationItems(int page, String url) throws IOException {

        List<PublicationContentList> publicationItems = new ArrayList<>();

        Document d = Jsoup.parse(
                getNetwork().request(
                        url + "&offset=" + ((page * 15) + 1),
                        null, "GET"
                ), "UTF-8", url
        );

        if (d.getElementsByClass("result-count").text().contains("Keine Treffer gefunden"))
            return null;

        Elements trs = d.getElementsByTag("tr");
        for (int i = 1; i < trs.size(); i++) {
            Element tr = trs.get(i);

            Element fundstelleTd = tr.getElementsByAttributeValue("data-label", "Fundstelle").first();

            String link =
                    fundstelleTd.getElementsByTag("a").first().attr("abs:href");

            String fundstelle = fundstelleTd.text();
            String titel = tr.getElementsByAttributeValue("data-label", "Titel").text();

            publicationItems.add(
                    new VerkuendungBayernContentList(fundstelle, titel, link)
            );

        }

        return publicationItems;
    }

    public abstract String getUrl();

    @Override
    public List<? extends ListItem> search(String query, int page) throws IOException {
        return getPublicationItems(page, getUrl() + "?query=" + URLEncoder.encode(query, "UTF-8"));
    }
}
