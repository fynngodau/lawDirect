package godau.fynn.lawdirect.providers.test;

import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.*;
import godau.fynn.lawdirect.model.parent.*;

import java.util.Arrays;
import java.util.List;

public class TestProvider extends LawProvider {

    @Override
    public List<ParentItem> getLawItems() {
        return Arrays.asList(
                new TestBook(),
                new TestPublication(),
                new ErrorProvider()
        );
    }

    @Override
    public String getTitle() {
        return "Test provider";
    }

    @Nullable
    @Override
    public String getDataSourceName() {
        return null;
    }

    @Override
    public String getId() {
        return "test";
    }

}
