package godau.fynn.lawdirect.providers.offline;

import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.parent.LawProvider;
import godau.fynn.lawdirect.model.ParentItem;

import java.util.List;

public class OfflineProvider extends LawProvider {

    public OfflineProvider() {
        overwriteContent(new OfflineParentItem("", this, null));
    }
    @Override
    public List<ParentItem> getLawItems() {
        return null;
    }

    @Override
    public String getTitle() {
        return "Offline storage";
    }

    @Nullable
    @Override
    public String getDataSourceName() {
        return null;
    }

    @Override
    public String getId() {
        return "";
    }
}
