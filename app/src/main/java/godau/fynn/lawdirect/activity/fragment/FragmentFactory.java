package godau.fynn.lawdirect.activity.fragment;

import android.os.Bundle;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.hierarchy.HierarchiesedContentList;
import godau.fynn.lawdirect.model.parent.ContentList;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.providers.LawProviderProvider;

public abstract class FragmentFactory {

    public static ContentFragment<? extends ListItem, ? extends ParentItem> getFragment(ParentItem parentItem, String path) {

        ContentFragment<? extends ListItem, ? extends ParentItem> contentFragment;

        if (parentItem instanceof LawProviderProvider) {
            contentFragment = new LawProviderProviderContentFragment();
        } else if (parentItem instanceof HierarchiesedContentList) {
            contentFragment = new HierarchiesedContentListContentFragment();
        } else if (parentItem instanceof ContentList) {
            contentFragment = new ContentListContentFragment();
        } else {
            contentFragment = new GenericContentFragment();
        }

        Bundle args = new Bundle();
        args.putSerializable("law_item", parentItem);
        args.putString("path", path);
        contentFragment.setArguments(args);
        return contentFragment;
    }
}
