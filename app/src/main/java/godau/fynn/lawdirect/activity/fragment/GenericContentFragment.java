package godau.fynn.lawdirect.activity.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.view.adapter.ListItemAdapter;

import java.util.List;

public class GenericContentFragment extends RecyclerViewAdapterContentFragment {

    private ProgressBar progressBar;

    private LinearLayoutManager layoutManager;
    private RecyclerView recyclerView;

    private boolean scrollListenerRegistered = false;

    protected @LayoutRes
    int getLayout() {
        return R.layout.content_generic;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        progressBar = view.findViewById(R.id.progress);

        recyclerView = view.findViewById(R.id.recycler_view);
        adapteredView = recyclerView;

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter = new ListItemAdapter(path, content));

        if (PreferenceManager.getDefaultSharedPreferences(recyclerView.getContext()).getString("layout_default", "row_list_item_with_arrow")
                .equals("row_list_item_with_arrow")) {
            recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
            recyclerView.setPadding(0, 0, 0, 0);
        }

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void onRecreate() {
        // Restore previous state
        if (scrollListenerRegistered) {
            recyclerView.addOnScrollListener(new LazyLoadScrollListener());
        }
    }

    @Override
    public void onBeginInitialLoad() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onEndInitialLoad() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void addContent(List<ListItem> items) {
        recyclerView.post(() -> {
            super.addContent(items);

            RecyclerView.OnScrollListener scrollListener = new GenericContentFragment.LazyLoadScrollListener();
            recyclerView.addOnScrollListener(scrollListener);
            scrollListenerRegistered = true;

            scrollListener.onScrolled(recyclerView, 0, 0);
        });


    }

    private class LazyLoadScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            if (layoutManager.findLastVisibleItemPosition() >= content.size() - 15) {
                recyclerView.clearOnScrollListeners();
                scrollListenerRegistered = false;
                loadMore();
            }
        }
    }
}
