package godau.fynn.lawdirect.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.activity.fragment.PreferencesFragment;

public class PreferencesActivity extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_holder);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment, new PreferencesFragment())
                .commit();
    }
}
