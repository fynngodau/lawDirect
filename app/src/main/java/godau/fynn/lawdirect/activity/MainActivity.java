package godau.fynn.lawdirect.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.activity.fragment.FragmentFactory;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.model.Searchable;
import godau.fynn.lawdirect.model.WebUrl;
import godau.fynn.lawdirect.persistence.ItemDao;
import godau.fynn.lawdirect.persistence.ItemDataDatabase;
import godau.fynn.lawdirect.providers.LawProviderProvider;
import godau.fynn.lawdirect.util.SearchResults;
import godau.fynn.librariesdirect.AboutLibrariesActivity;
import godau.fynn.librariesdirect.Library;
import godau.fynn.librariesdirect.License;

public class MainActivity extends FragmentActivity {

    private MenuItem searchItem;
    private MenuItem openExternallyItem;

    private Searchable searchProvider;
    private WebUrl webUrlProvider;
    private String path;

    private ItemDao items;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_holder);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment, FragmentFactory.getFragment(new LawProviderProvider(), ""))
                .commit();

    }

    @Override
    public void onBackPressed(){
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onMenuItemSelected(int featureId, @NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_about:
                Intent intent = new AboutLibrariesActivity.IntentBuilder(this).setLibraries(new Library[]{
                        new Library("jsoup", License.MIT_LICENSE, "Copyright © 2009 - 2019 Jonathan Hedley (jonathan@hedley.net)", "Jonathan Hedley", "https://jsoup.org"),
                        new Library("Picasso", License.APACHE_20_LICENSE, null, "Square, Inc.", "https://square.github.io/picasso/"),
                        new Library("android-textview-html-list", License.APACHE_20_LICENSE, "Copyright 2013-2015 Juha Kuitunen", "Juha Kuitunen", "https://bitbucket.org/Kuitsi/android-textview-html-list"),
                        new Library("BreadcrumbsView", License.MIT_LICENSE, "MIT LICENSE\n\nCopyright (c) 2017-2018 Fung Go (fython)", "Fung Go", "https://github.com/fython/BreadcrumbsView"),
                        new Library("TypedRecyclerView", License.CC0_LICENSE, null, "Fynn Godau", "https://codeberg.org/fynngodau/TypedRecyclerView"),
                        new Library("librariesDirect", License.CC0_LICENSE, null, "Fynn Godau", "https://codeberg.org/fynngodau/librariesDirect"),
                }).build();
                startActivity(intent);
                break;
            case R.id.menu_preferences:
                startActivityForResult(new Intent(this, PreferencesActivity.class), 0);
                break;
            case R.id.menu_search:
                View view = getLayoutInflater().inflate(R.layout.popup_edittext, null, false);
                new AlertDialog.Builder(this)
                        .setTitle(R.string.menu_search)
                        .setView(view)
                        .setNegativeButton(R.string.cancel, null)
                        .setPositiveButton(R.string.go, (dialogInterface, i) -> {
                            EditText editText = view.findViewById(R.id.edit);
                            String search = editText.getText().toString();

                            getSupportFragmentManager()
                                    .beginTransaction()
                                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_right)
                                    .replace(R.id.fragment, FragmentFactory.getFragment(
                                            new SearchResults(searchProvider, search), path
                                    ))
                                    .addToBackStack(null)
                                    .commit();

                        }).show();
                break;
            case R.id.menu_open_externally:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(webUrlProvider.getUrl()));
                startActivity(intent);
                break;
        }

        return super.onMenuItemSelected(featureId, item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        searchItem = menu.findItem(R.id.menu_search);
        searchItem.setEnabled(searchProvider != null);
        openExternallyItem = menu.findItem(R.id.menu_open_externally);
        openExternallyItem.setEnabled(webUrlProvider != null);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == 0) {
            recreate();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void setParentItem(ParentItem parentItem, String path) {

        // Set search provider

        if (parentItem instanceof Searchable) {
            searchProvider = (Searchable) parentItem;
        } else {
            searchProvider = null;
        }

        if (searchItem != null) {
            searchItem.setEnabled(searchProvider != null);
        }

        // Set web url provider

        if (parentItem instanceof WebUrl) {
            webUrlProvider = (WebUrl) parentItem;
        } else {
            webUrlProvider = null;
        }

        if (openExternallyItem != null) {
            openExternallyItem.setEnabled(webUrlProvider != null);
        }

        // Set path
        this.path = path;
    }

    public ItemDao getItemDao() {

        if (items == null) {
            items = ItemDataDatabase.getItemDao(this);
        }

        return items;
    }
}
