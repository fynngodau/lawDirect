package godau.fynn.lawdirect.activity.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import godau.fynn.lawdirect.activity.MainActivity;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.providers.LawProviderProvider;

import java.io.IOException;
import java.util.List;

public abstract class ContentFragment<L extends ListItem, P extends ParentItem> extends Fragment {

    protected P parentItem;
    protected String path = "";

    private int page = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey("law_item")) {
            parentItem = (P) getArguments().getSerializable("law_item");
        } else {
            parentItem = (P) new LawProviderProvider();
        }

        parentItem.setContext(getContext());

        if (getArguments() != null && getArguments().containsKey("path")) {
            path = getArguments().getString("path");
            if (!parentItem.getId().isEmpty()) {
                path += "/" + parentItem.getId();
            }
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayout(), container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Set action bar title
        getActivity().getActionBar().setTitle(parentItem.getTitle());
        ((MainActivity) getActivity()).setParentItem(parentItem, path);
    }

    /**
     * Load the next (possibly first) page. Must not be called when loading
     * is already in progress.
     */
    protected void loadMore() {
        loadContent(parentItem, page++);
    }

    private void loadContent(final ParentItem parentItem, final int page) {

        Log.d(ContentFragment.class.getSimpleName(), "loading page " + page);
        long startMillis = System.currentTimeMillis();

        if (page == 0) {
            onBeginInitialLoad();
        } else {
            onBeginLazyLoad();
        }

        final Handler handler = new Handler(Looper.getMainLooper());

        new Thread(() -> {

            List<L> listItems = null;
            IOException exception = null;

            try {

                // Download content
                listItems = (List<L>) parentItem.getDisplayContent(page);

            } catch (IOException e) {

                exception = e;

                // For retrying, decrease page count again
                ContentFragment.this.page--;

                e.printStackTrace();
            }

            final List<L> finalListItems = listItems;
            final IOException finalException = exception;
            handler.post(() -> {

                // End load
                if (page == 0) {
                    onEndInitialLoad();
                } else {
                    onEndLazyLoad();
                }

                // Add content
                if (finalListItems != null) {
                    addContent(finalListItems);
                }

                // Display network error
                if (finalException != null) {
                    onNetworkError(finalException);
                }

                long duration = System.currentTimeMillis() - startMillis;
                Log.d(ContentFragment.class.getSimpleName(), "loaded page " + page + " within " + duration + "ms");
            });

        }).start();
    }

    protected abstract @LayoutRes
    int getLayout();

    /**
     * Called before initially beginning to load
     */
    public void onBeginInitialLoad() {}

    /**
     * Called after the first load is complete, but before
     * {@link #addContent(List)}
     */
    public void onEndInitialLoad() {}

    /**
     * Called before more content is lazy loaded
     */
    public void onBeginLazyLoad() {}

    /**
     * Called once more content has been lazy loaded, but before
     * {@link #addContent(List)}
     */
    public void onEndLazyLoad() {}

    /**
     * Called when a network error occurs while loading. Guaranteed
     * to only be called after loading has ended. No need to print
     * the stack trace, this has already been done; implementations
     * of this method are only responsible for exposing this exception
     * to the graphical interface.
     * @param e The IOException that has caused this network error.
     */
    public abstract void onNetworkError(IOException e);

    /**
     * Called when more content has been loaded. There is no
     * guarantee that this method is called once loading is done,
     * but it will only be called after loading has been ended.
     *
     * @param items New content
     */
    public abstract void addContent(List<L> items);
}
