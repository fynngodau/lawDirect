package godau.fynn.lawdirect.activity.fragment;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import androidx.preference.CheckBoxPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.parent.LawProvider;
import godau.fynn.lawdirect.providers.LawProviderProvider;

public class PreferencesFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, null);

        PreferenceCategory category = findPreference("cat_active_providers");

        for (LawProvider lawProvider : new LawProviderProvider().getAllLawProviders()) {

            final Preference preference = new CheckBoxPreference(getContext());
            preference.setTitle(lawProvider.getTitle());
            preference.setKey("enable_provider_" + lawProvider.getId());
            preference.setDefaultValue(true);

            Picasso.get()
                    .load(lawProvider.getFavicon())
                    .resize(toPx(24), toPx(24))
                    .into(new PreferenceTarget(preference));

            category.addPreference(preference);

        }
    }

    public int toPx(int dp) {
        return (int) (dp * ((float) getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    /**
     * Utility class for inserting downloaded image into preference
     * using Picasso
     */
    private class PreferenceTarget implements Target {

        private final Preference preference;

        public PreferenceTarget(Preference preference) {
            this.preference = preference;
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            preference.setIcon(new BitmapDrawable(getResources(), bitmap));
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    }
}
