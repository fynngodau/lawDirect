package godau.fynn.lawdirect.activity.fragment;

import android.os.Bundle;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.parent.LawProvider;
import godau.fynn.lawdirect.providers.LawProviderProvider;
import godau.fynn.lawdirect.view.adapter.LawProviderProviderAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LawProviderProviderContentFragment extends ContentFragment<LawProvider, LawProviderProvider> {

    private LawProviderProviderAdapter adapter;
    private RecyclerView recyclerView;

    private final List<LawProvider> content = new ArrayList<>();

    @Override
    protected int getLayout() {
        return R.layout.content_generic;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter = new LawProviderProviderAdapter(content));

        recyclerView.setPadding(0, 0, 0, 0);

        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        if (content.size() == 0) {
            // Begin loading
            loadMore();
        }
    }

    @Override
    public void onNetworkError(IOException e) {
        // Cannot occur
    }

    @Override
    public void addContent(List<LawProvider> items) {
        recyclerView.post(() -> {
            content.addAll(items);
            adapter.notifyDataSetChanged();
        });
    }
}
