package godau.fynn.lawdirect.activity.fragment;

import android.os.Bundle;
import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.util.indicator.NetworkErrorIndicatorItem;
import godau.fynn.lawdirect.view.adapter.ListItemAdapter;
import godau.fynn.lawdirect.util.indicator.LoadingIndicatorItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class RecyclerViewAdapterContentFragment extends ContentFragment<ListItem, ParentItem> {

    protected final List<ListItem> content = new ArrayList<>();
    protected ListItemAdapter adapter;

    /**
     * View that the adapter is attached to. Data changes will be posted
     * to this view.
     */
    protected View adapteredView;

    /**
     * Implementations of this method are responsible for setting the adapter as well as
     * {@link #adapteredView} field. Implementations must also call this super method.
     */
    @CallSuper
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        if (content.size() == 0) {
            // Begin loading
            loadMore();
        } else {
            // View recreated
            onRecreate();
        }
    }

    protected void onRecreate() {}

    @Override
    public void onBeginInitialLoad() {
        onBeginLazyLoad();
    }

    @Override
    public void onEndInitialLoad() {
        onEndLazyLoad();
    }

    @Override
    public void onBeginLazyLoad() {

        adapteredView.post(() -> {
            // Add loading indicator
            content.add(new LoadingIndicatorItem(adapteredView.getContext()));
            adapter.notifyItemInserted(content.size() - 1);
        });
    }

    @Override
    public void onEndLazyLoad() {

        int indicatorPosition = content.size() - 1;
        adapteredView.post(() -> {
            // Remove loading indicator
            content.remove(indicatorPosition);
            adapter.notifyItemRemoved(indicatorPosition);
        });
    }

    /**
     * Only call this method in a <code>post(Runnable)</code> on {@link #adapteredView}
     *
     * @param items New content
     */
    @CallSuper
    @Override
    public void addContent(List<ListItem> items) {

        // Add new content
        int oldSize = content.size();
        content.addAll(items);

        adapter.notifyItemRangeInserted(oldSize, items.size());
    }

    @Override
    public void onNetworkError(IOException e) {

        adapteredView.post(() -> {
            content.add(new NetworkErrorIndicatorItem(getContext(), () -> adapteredView.post(() -> {
                // Assume network error to be last item
                content.remove(content.size() - 1);
                adapter.notifyItemRemoved(content.size());
                loadMore();
            })));
            adapter.notifyItemInserted(content.size() - 1);
        });
    }
}
