package godau.fynn.lawdirect.activity.fragment;

import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.view.adapter.ContentListAdapter;

import java.util.List;

public class ContentListContentFragment extends RecyclerViewAdapterContentFragment {

    protected ViewPager2 viewPager;
    private TabLayout tabs;

    private final LazyLoadCallback callback = new LazyLoadCallback();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);

        // Add tab layout and let it have its required AppCompat theme
        FrameLayout frame = view.findViewById(R.id.tab_frame);
        tabs = new TabLayout(new ContextThemeWrapper(getContext(), R.style.Theme_AppCompat_Light));
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        frame.addView(tabs);

        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.content_paragraph_list;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewPager = view.findViewById(R.id.viewpager);
        adapteredView = viewPager;

        viewPager.setAdapter(adapter = new ContentListAdapter(path, content));

        new TabLayoutMediator(tabs, viewPager,
                (tab, position) -> tab.setText(adapter.getTitle(position))
        ).attach();

        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                boolean moreThanOnePage = adapter.getItemCount() > 1;
                viewPager.setUserInputEnabled(moreThanOnePage);
                tabs.setVisibility(moreThanOnePage? View.VISIBLE : View.GONE);
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                onChanged();
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                onChanged();
            }
        });


        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void addContent(List<ListItem> items) {
        viewPager.post(() -> {
            super.addContent(items);

            viewPager.registerOnPageChangeCallback(callback);

            callback.onPageScrolled(viewPager.getCurrentItem(), 0, 0);
        });
    }

    private class LazyLoadCallback extends ViewPager2.OnPageChangeCallback {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (position + 1 >= content.size() - 15) {
                viewPager.unregisterOnPageChangeCallback(callback);
                loadMore();
            }
        }
    }
}
