package godau.fynn.lawdirect.activity.fragment;

import android.os.Bundle;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager2.widget.ViewPager2;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.hierarchy.Heading;
import godau.fynn.lawdirect.model.hierarchy.HierarchisedContentItem;
import moe.feng.common.view.breadcrumbs.BreadcrumbsCallback;
import moe.feng.common.view.breadcrumbs.BreadcrumbsView;
import moe.feng.common.view.breadcrumbs.model.BreadcrumbItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HierarchiesedContentListContentFragment extends ContentListContentFragment {

    private BreadcrumbsView breadcrumbs;
    private final BreadcrumbCallback breadcrumbCallback = new BreadcrumbCallback();

    private int currentPosition = 0;

    @Override
    protected int getLayout() {
        return R.layout.content_hierarchiesed;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        breadcrumbs = view.findViewById(R.id.breadcrumbs);
        viewPager.registerOnPageChangeCallback(breadcrumbCallback);

        breadcrumbs.setCallback(new BreadcrumbsCallback<Object>() {
            @Override
            public void onItemClick(@NonNull BreadcrumbsView view, int position) {
                // Move back from current position to last header which is exactly this deep in hierarchy
                ListItem item;
                while (!((item = content.get(currentPosition--)) instanceof Heading && ((Heading) item).getPositionInHierarchy().length - 1 == position)) {
                }

                viewPager.setCurrentItem(currentPosition);
                breadcrumbCallback.onPageScrolled(++currentPosition, 0, 0);
            }

            @Override
            public void onItemChange(@NonNull BreadcrumbsView view, int parentPosition, @NonNull Object nextItem) {

            }
        });
    }

    private class BreadcrumbCallback extends ViewPager2.OnPageChangeCallback {

        private final List<String> breadcrumbTracker = new ArrayList<>();

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            if (positionOffset > 0.5f) {
                position++;
            }

            if (position == currentPosition) {
                return;
            } else currentPosition = position;

            if (content.get(position) instanceof HierarchisedContentItem) {
                HierarchisedContentItem item = (HierarchisedContentItem) content.get(position);

                // Find unchanged breadcrumb items
                int same = 0;
                for (String breadcrumb : item.getBreadcrumbs()) {
                    if (breadcrumbTracker.size() > same && breadcrumb.equals(breadcrumbTracker.get(same))) {
                        same++;
                    } else {
                        break;
                    }
                }

                // Add new breadcrumbs to view to match item breadcrumbs
                if (same < item.getBreadcrumbs().length | same < breadcrumbTracker.size()) {
                    breadcrumbs.removeItemAfter(same);

                    breadcrumbTracker.subList(same, breadcrumbTracker.size()).clear();

                    for (int i = same; i < item.getBreadcrumbs().length; i++) {
                        breadcrumbs.addItem(new BreadcrumbItem(Collections.singletonList(item.getBreadcrumbs()[i])));
                        breadcrumbTracker.add(item.getBreadcrumbs()[i]);
                    }
                }
            }
        }
    }
}
