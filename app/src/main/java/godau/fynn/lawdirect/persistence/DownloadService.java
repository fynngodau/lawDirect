package godau.fynn.lawdirect.persistence;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.ParentItem;

import java.io.IOException;

public class DownloadService extends Service {

    public static final String EXTRA_PATH = "path",
            EXTRA_ITEM = "item";
    private static final String NOTIFICATION_CHANNEL_ID = "download";

    private static final int NOTIFICATION_ID = 3696; // DOWN on a numpad
    private static final int ERROR_NOTIFICATION_ID = 36963; // DOWNE on a numpad
    private static final int SUCCESS_NOTIFICATION_ID = 36967; // DOWNS on a numpad

    private String path;
    private ParentItem item;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        path = intent.getStringExtra(EXTRA_PATH);
        item = (ParentItem) intent.getSerializableExtra(EXTRA_ITEM);

        startForeground(NOTIFICATION_ID, postNotification(
                createNotification(createNotificationBuilder(0d, true))
        ));

        new Thread(() -> {
            try {
                new Downloader(path, item, this)
                        .addObserver(aDouble -> postNotification(createNotification(createNotificationBuilder(aDouble, false))))
                        .run();
                postSuccessNotification();
            } catch (IOException e) {
                e.printStackTrace();
                postNetworkErrorNotification();
            } finally {
                stopSelf();
            }
        }).start();


        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Notification postNotification(Notification n) {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, n);

        return n;
    }

    private void postNetworkErrorNotification() {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ERROR_NOTIFICATION_ID, createNotification(
                createEmptyNotificationBuilder()
                .setContentTitle(getString(R.string.download_network_error))
                .setContentText(getString(R.string.download_network_error_detail, item.getTitle()))
                .setSmallIcon(R.drawable.broken_paragraph)
        ));
    }

    private void postSuccessNotification() {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(SUCCESS_NOTIFICATION_ID, createNotification(
                createEmptyNotificationBuilder()
                        .setContentTitle(getString(R.string.download_complete))
                        .setContentText(getString(R.string.download_complete_detail, item.getTitle()))
                        .setSmallIcon(R.drawable.broken_paragraph)
        ));
    }


    private Notification createNotification(Notification.Builder n) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            return n.build();
        } else {
            return n.getNotification();
        }
    }

    private Notification.Builder createNotificationBuilder(double progress, boolean indeterminate) {

        return createEmptyNotificationBuilder()
                .setOngoing(true)
                .setContentTitle(getString(R.string.download_progress, item.getTitle()))
                .setSmallIcon(R.drawable.broken_paragraph)
                .setProgress(100000, (int) (progress * 100000), indeterminate);
    }

    private Notification.Builder createEmptyNotificationBuilder() {
        createNotificationChannel();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return new Notification.Builder(this, NOTIFICATION_CHANNEL_ID);
        } else {
            return new Notification.Builder(this);
        }
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.download_channel_name);
            String description = getString(R.string.channel_description);

            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            channel.setSound(null, new AudioAttributes.Builder().build());

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
