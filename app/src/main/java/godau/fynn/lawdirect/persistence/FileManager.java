package godau.fynn.lawdirect.persistence;

import android.content.Context;
import android.util.Log;
import androidx.preference.PreferenceManager;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.model.parent.PublicationContentList;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileManager {

    private final Context context;

    private final ItemDao items;

    private final boolean assumePublicationsUnchanged;

    public FileManager(Context context) {
        this.context = context;
        this.items = ItemDataDatabase.getItemDao(context);
        assumePublicationsUnchanged = PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean("download_assume_unchanged_publications", true);
    }

    public void save(String path, ListItem item) throws IOException {
        File directory = new File(context.getFilesDir(), path);
        directory.mkdirs();

        save(path, directory, item);
    }

    public void save(String path, File directory, ListItem item) throws IOException {
        File file = new File(directory, item.getId());
        file.createNewFile();

        saveToFile(path, file, item);
    }

    /**
     * Saves item into the provided File object. Assumes <code>file</code> to exist.
     * Writes item data to database.
     *
     * @param file A File object that represents a file.
     * @param item Item to be serialized into file
     */
    private void saveToFile(String path, File file, ListItem item) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(item);
        objectOutputStream.close();
        fileOutputStream.close();

        writeToDatabase(path, item);
    }

    /**
     * @return File object for directory that this parent's childs must be saved in
     */
    public File saveParent(String path, File directory, ParentItem item) throws IOException {

        File newDirectory = new File(directory, item.getId());
        newDirectory.mkdir();

        File file = new File(newDirectory, "__THIS__");
        file.createNewFile();

        saveToFile(path, file, item);

        return newDirectory;
    }

    /**
     * @return Whether this parent item has already been saved
     */
    public boolean exists(File directory, ParentItem item) {
        File itemDirectory = new File(directory, item.getId());
        return itemDirectory.isDirectory();
    }

    public ListItem read(String id) throws IOException, ClassNotFoundException {
        return read(new File(context.getFilesDir(), id));
    }

    public ListItem read(File f) throws IOException, ClassNotFoundException, InvalidClassException {
        FileInputStream fileInputStream = new FileInputStream(f);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Object listItem = objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        return (ListItem) listItem;
    }

    public void saveRecursively(String path, File directory, ParentItem parent, ProgressConsumer consumer) throws IOException {

        if (assumePublicationsUnchanged && parent instanceof PublicationContentList && exists(directory, parent)) {
            // Skip this publication paragraph list item and only save position
            Log.d("DOWNLOAD", "updating position of " + parent.getTitle());
            writeToDatabase(path, parent);
            return;
        }

        Log.d("DOWNLOAD", "Saving " + parent.getTitle());

        // Save parent item
        directory = saveParent(path, directory, parent);
        path += "/" + parent.getId();

        List<ListItem> list = new ArrayList<>();
        List<? extends ListItem> addList;

        // Load list of all items to download
        int page = 0;
        do {
            try {
                addList = parent.getDisplayContent(page++);
            } catch (IOException e) {
                // Retry once
                Log.d("DOWNLOAD", "retrying");
                addList = parent.getDisplayContent(page - 1);
            }

            if (addList != null) {
                list.addAll(addList);
            } else {
                break;
            }

        } while (true);

        consumer.counted(list.size());

        for (ListItem item : list) {

            if (item instanceof ParentItem) {
                ((ParentItem) item).setContext(context);

                consumer.down();
                saveRecursively(path, directory, (ParentItem) item, consumer);
                consumer.up();

            } else {
                save(path, directory, item);
                consumer.progress();
            }
        }
    }

    public void writeToDatabase(String path, ListItem item) {
        items.insert(new ItemData(path, item));
    }
}
