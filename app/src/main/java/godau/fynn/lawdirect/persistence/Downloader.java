package godau.fynn.lawdirect.persistence;

import android.content.Context;
import android.util.Log;
import androidx.core.util.Consumer;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.util.Resolver;

import java.io.File;
import java.io.IOException;

public class Downloader {

    protected final String path;
    protected final ParentItem root;
    protected final Context context;
    private ProgressConsumer progressConsumer = new ProgressConsumer();

    public Downloader(String path, ParentItem root, Context context) {
        this.path = path;
        this.root = root;
        this.context = context;
    }

    public Downloader addObserver(Consumer<Double> observer) {
        progressConsumer.addObserver(observer);
        return this;
    }

    public void run() throws IOException {

        root.setContext(context);

        downloadPath();
        operation();
    }

    protected void downloadPath() throws IOException {
        Resolver.resolve(path, new Consumer<ParentItem>() {

            private String path = "";

            @Override
            public void accept(ParentItem item) {
                try {
                    new ParentOnly(path, item, context).run();
                } catch (IOException e) {
                    // Not a network exception but rather an exception when saving to file
                    e.printStackTrace();
                }
                path += "/" + item.getId();
            }
        });
    }

    protected void operation() throws IOException {
        new FileManager(context).saveRecursively(path, new File(context.getFilesDir(), path), root, progressConsumer);
    }

    public static class ParentOnly extends Downloader {

        public ParentOnly(String path, ParentItem root, Context context) {
            super(path, root, context);
        }

        @Override
        public void operation() throws IOException {
            Log.d("DOWNLOAD", "Saving parent: " + root.getTitle());
            new FileManager(context).saveParent(path, new File(context.getFilesDir(), path), root);
        }
    }
}
