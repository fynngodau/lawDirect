package godau.fynn.lawdirect.persistence;

import android.text.Spanned;
import android.text.SpannedString;
import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import godau.fynn.lawdirect.model.ListItem;

@Entity(tableName = "metadata")
public class ItemData {

    String searchableText, positionId;

    @PrimaryKey
    @NonNull
    String id;

    ItemData() {}

    ItemData(String path, ListItem item) {
        String title = item.getTitle();
        if (title == null) {
            title = "";
        }
        Spanned text = item.getText();
        if (text == null) {
            text = new SpannedString("");
        }
        searchableText = title + "\n\n" + text;

        positionId = item.getPositionId();

        id = path + "/" + item.getId();
    }

    public String getId() {
        return id;
    }
}
