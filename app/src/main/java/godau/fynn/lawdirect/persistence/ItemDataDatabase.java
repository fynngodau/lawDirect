package godau.fynn.lawdirect.persistence;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = ItemData.class, version = 3)
public abstract class ItemDataDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "database";

    public abstract ItemDao getItemDao();

    public static ItemDao getItemDao(Context context) {
        return Room.databaseBuilder(context, ItemDataDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigrationFrom(1, 2)
                .build()
                .getItemDao();
    }

}
