package godau.fynn.lawdirect.persistence;

import androidx.core.util.Consumer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProgressConsumer {

    private final List<Integer> amounts = new ArrayList<>();
    private final List<Integer> progresses = new ArrayList<>(Collections.singletonList(0));

    private final List<Consumer<Double>> observers = new ArrayList<>();

    int hierarchyPosition = 0;

    /**
     * Move down in (into) hierarchy
     */
    public void down() {
        hierarchyPosition++;
        progresses.add(0);

        notifyObservers();
    }

    /**
     * Move up in (out of) hierarchy; includes call to {@link #progress()}
     */
    public void up() {
        progresses.remove(hierarchyPosition);
        if (amounts.size() > hierarchyPosition) {
            amounts.remove(hierarchyPosition);
        }
        hierarchyPosition--;
        // Item one step down in hierarchy is downloaded → item is done
        // This call will also call notifyObservers()
        progress();
    }

    /**
     * Record amount of items to be downloaded in hierarchy
     */
    public void counted(int amount) {
        amounts.add(hierarchyPosition, amount);

        notifyObservers();
    }

    /**
     * Record progression by one item
     */
    public void progress() {
        int p = progresses.get(hierarchyPosition);
        progresses.set(hierarchyPosition, ++p);

        notifyObservers();
    }

    private double getProgress() {

        double sum = 0, factor = 1;

        for (int i = 0; i <= hierarchyPosition && i < amounts.size() && i < progresses.size(); i++) {
            factor /= amounts.get(i);
            sum += factor * progresses.get(i);
        }

        return sum;
    }

    public void addObserver(Consumer<Double> consumer) {
        observers.add(consumer);
    }

    private void notifyObservers() {
        for (Consumer<Double> consumer : observers) {
            consumer.accept(getProgress());
        }
    }
}
