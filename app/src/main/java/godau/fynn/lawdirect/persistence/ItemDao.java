package godau.fynn.lawdirect.persistence;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public abstract class ItemDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(List<ItemData> items);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(ItemData item);

    @Query("SELECT id FROM metadata WHERE id LIKE :fullId || '/%' ORDER BY positionId")
    abstract List<String> getDescendantIds(String fullId);

    public List<String> getChildIds(String fullId) {
        List<String> descendants = getDescendantIds(fullId);

        for (int i = descendants.size() - 1; i >= 0; i--) {
            String item = descendants.get(i);
            String remainingPath = item.substring(fullId.length() + 1);
            if (remainingPath.contains("/")) {
                descendants.remove(i);
            }
        }

        return descendants;
    }

/*    @Transaction
    @Query("SELECT * FROM " +
            "(SELECT * FROM listItem WHERE id LIKE :path || '%')" +
            "WHERE LOWER(title) LIKE LOWER('%' || :query || '%')" +
            "OR LOWER(text) LIKE LOWER('%' || :query || '%')" +
            "ORDER BY positionId")
    public abstract List<DownloadedParentItem> searchIn(String query, String path);
*/
    /**
     * Like search, but returns the parents items of items that are not
     * parent items instead
     *//*
    @Transaction
    public List<ListItem> searchParentsIn(String query, String path) {

        Set<ListItem> stubList = new LinkedHashSet<>();
        Set<String> parentQueries = new LinkedHashSet<>();

        int stubs = 0;
        for (DownloadedParentItem downloadedParentItem : searchIn(query, path)) {
            if (downloadedParentItem.getContent().size() > 0) {
                stubList.add(new DatabaseParentItem(downloadedParentItem));
            } else {
                boolean added = parentQueries.add(downloadedParentItem.getListItem().getParentId());
                if (added) {
                    stubList.add(new Stub(stubs++));
                }
            }
        }

        // Query list of needed parent items
        List<DownloadedParentItem> parentItems = getParentItemsById(parentQueries);

        Set<ListItem> listItems = new LinkedHashSet<>();

        Iterator<DownloadedParentItem> parentItemIterator = parentItems.iterator();
        for (ListItem stubItem : stubList) {
            if (stubItem instanceof Stub) {
                listItems.add(new DatabaseParentItem(parentItemIterator.next()));
            } else {
                listItems.add(stubItem);
            }
        }


        return new ArrayList<>(listItems);
    }

    private static final class Stub extends BaseItem {

        private final int id;
        public Stub(int id) {
            this.id = id;
        }

        @Override
        public String getTitle() {
            return null;
        }

        @Nullable
        @Override
        public Spanned getText() {
            return null;
        }

        @Override
        public String getId() {
            return String.valueOf(id);
        }
    }
    */
}
