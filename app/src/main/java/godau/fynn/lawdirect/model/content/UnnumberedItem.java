package godau.fynn.lawdirect.model.content;

import androidx.annotation.Nullable;

/**
 * TODO: The current database implementation only
 * supports one unnumbered item per paragraph list.
 */
public abstract class UnnumberedItem extends ContentItem {

    public UnnumberedItem(@Nullable String title) {
        super(title);
    }

    @Nullable
    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getId() {
        return "item";
    }
}