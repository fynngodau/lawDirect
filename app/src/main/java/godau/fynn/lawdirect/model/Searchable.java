package godau.fynn.lawdirect.model;

import java.io.IOException;
import java.util.List;

/**
 * {@link ParentItem}s that implement this interface can be searched for their content.
 */
public interface Searchable extends ParentItem {

    List<? extends ListItem> search(String query, int page) throws IOException;

}
