package godau.fynn.lawdirect.model.parent;

import android.text.Spanned;
import android.text.SpannedString;
import godau.fynn.lawdirect.model.ParentItem;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Represents a publication consisting of legal documents, like an Amtsblatt.
 *
 * Publications <b>must</b> be ordered chronologically by release date and <b>must</b>
 * add new items at the front.
 */
public abstract class Publication extends BaseParentItem implements ParentItem, Serializable {

    /**
     * @see #getContent(int)
     */
    public abstract List<PublicationContentList> getPublicationItems(int page) throws IOException;

    public abstract String getShortcode();

    public List<PublicationContentList> getContent(int page) throws IOException {
        List<PublicationContentList> paragraphLists = getPublicationItems(page);

        if (paragraphLists != null) for (int i = 0; i < paragraphLists.size(); i++) {
            paragraphLists.get(i).setPositionId(((long) page << 32) + i);
        }
        return paragraphLists;
    }

    public Spanned getText() {
        return new SpannedString(getShortcode());
    }

    public String getId() {
        return getShortcode();
    }
}
