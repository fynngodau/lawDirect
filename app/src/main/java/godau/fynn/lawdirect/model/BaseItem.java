package godau.fynn.lawdirect.model;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.network.Network;

import java.io.Serializable;

/**
 * Default implementation of {@link ListItem}. It provides context and network to descendant classes
 * and implements {@link #equals(Object)} as well as {@link #hashCode()} and maps {@link #getPositionId()}
 * to {@link #getId()}.
 */
public abstract class BaseItem implements ListItem, Serializable {

    @Override
    public String getPositionId() {
        return getId();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof ListItem) {
            return getId().equals(((ListItem) obj).getId());
        } else {
            return super.equals(obj);
        }
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
