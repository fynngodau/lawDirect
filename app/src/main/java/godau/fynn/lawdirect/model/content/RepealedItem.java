package godau.fynn.lawdirect.model.content;

import android.content.Context;
import android.text.Spanned;
import android.text.SpannedString;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.hierarchy.NumberedItem;

public class RepealedItem extends NumberedItem {

    private final String id;
    private final String text;

    public RepealedItem(String number, @Nullable String title, String id, Context context) {
        super(number, title);
        this.id = id;
        this.text = context.getString(R.string.repealed);
    }

    @Override
    public Spanned getText() {
        return new SpannedString(text);
    }

    @Override
    public String getId() {
        return id;
    }
}
