package godau.fynn.lawdirect.model.content;

import android.text.Spanned;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.BaseItem;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.parent.ContentList;

import java.util.Locale;

/**
 * Content items are items that are contained in a content list.
 * <p>Content items may for example be numbered items, unnumbered items, repealed items and headings.</p>
 *
 * @see ContentList
 */
public abstract class ContentItem extends BaseItem implements ListItem {
    protected @Nullable String title;

    private long id;

    public ContentItem(@Nullable String title) {
        this.title = title;
    }

    public String getTitle() {
        if (title == null)
            return "";
        else
            return title;
    }

    public abstract Spanned getText();

    public void setPositionId(long id) {
        this.id = id;
    }

    @Override
    public String getPositionId() {
        return String.format(Locale.ENGLISH, "%064d", id);
    }
}
