package godau.fynn.lawdirect.model;

import android.content.Context;
import androidx.annotation.Nullable;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Every {@link ListItem} that can be "opened" to reveal more items must implement this
 * interface.
 *
 * @see godau.fynn.lawdirect.model.parent.BaseParentItem
 * @see ListItem
 * @see godau.fynn.lawdirect.model.parent.SinglePageParentItem
 */
public interface ParentItem extends ListItem {

    /**
     * Returns content or the content that overwrote the content from offline storage
     *
     * @param page Index of the page to load; the first page is 0
     * @return <code>null</code> if the page doesn't exist, a list of its content otherwise
     */
    public @Nullable List<? extends ListItem> getDisplayContent(int page) throws IOException;

    /**
     * Returns the direct descendant of this item or of its overwrite item that has
     * this id ("Go To" method)
     */
    public @Nullable ListItem getDisplayItem(String id) throws IOException;

    public void setContext(Context context);
}
