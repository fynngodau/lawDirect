package godau.fynn.lawdirect.model.hierarchy;

import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.content.ContentItem;

public abstract class HierarchisedContentItem extends ContentItem {

    private int[] position;
    private String[] breadcrumbs;

    public HierarchisedContentItem(@Nullable String title) {
        super(title);
    }

    void setPositionInHierarchy(int[] position, String[] breadcrumbs) {
        this.position = position;
        this.breadcrumbs = breadcrumbs;
    }

    public int[] getPositionInHierarchy() {
        return position;
    }

    public String[] getBreadcrumbs() {
        return breadcrumbs;
    }

}
