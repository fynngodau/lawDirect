package godau.fynn.lawdirect.model.hierarchy;

import android.text.Spanned;
import android.text.SpannedString;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.model.parent.ContentList;

/**
 * Object representing a law statue book
 */
public abstract class Book extends HierarchiesedContentList {
    public abstract String getShortcode();

    public String getId() {
        return getShortcode();
    }

    public Spanned getText() {
        return new SpannedString(getShortcode());
    }
}
