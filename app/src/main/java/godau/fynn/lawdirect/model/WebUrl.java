package godau.fynn.lawdirect.model;

/**
 * Parents that could also be opened in browser should implement
 * this method and return the corresponding url
 */
public interface WebUrl extends ParentItem {

    String getUrl();
}
