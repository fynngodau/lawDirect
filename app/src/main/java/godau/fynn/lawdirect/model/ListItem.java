package godau.fynn.lawdirect.model;

import android.content.Context;
import android.text.Spanned;
import androidx.annotation.Nullable;

import java.io.Serializable;

/**
 * Every item that should be displayed in any way must implement this interface.
 *
 * <p>Take care: every <code>ListItem</code> must be {@link Serializable}! A list
 * item may not have fields that are not <code>Serializable</code>.</p>
 *
 * @see BaseItem
 * @see ParentItem
 */
public interface ListItem extends Serializable {
    public String getTitle();

    public @Nullable Spanned getText();

    public String getId();

    /**
     * Position to place this item in its parent
     */
    public String getPositionId();
}
