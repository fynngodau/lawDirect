package godau.fynn.lawdirect.model.parent;

import godau.fynn.lawdirect.model.*;
import godau.fynn.lawdirect.model.content.ContentItem;
import godau.fynn.lawdirect.model.hierarchy.Book;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * A content list contains content items. A content list is usually a child item of {@link Book} or
 * {@link Publication}.
 *
 * @see ContentItem
 */
public abstract class ContentList extends BaseParentItem implements ListItem, ParentItem, Serializable {

    /**
     * @see #getContent(int)
     */
    protected abstract List<? extends ContentItem> getContentItems(int page) throws IOException;

    public abstract String getTitle();

    public List<? extends ContentItem> getContent(int page) throws IOException {
        List<? extends ContentItem> contentItems = getContentItems(page);

        if (contentItems != null) for (int i = 0; i < contentItems.size(); i++) {
            contentItems.get(i).setPositionId(((long) page << 32) + i);
        }
        return contentItems;
    }
}
