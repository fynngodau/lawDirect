package godau.fynn.lawdirect.model.parent;

import java.util.Locale;

/**
 * A ContentList that is inside a Publication. When downloading, it is assumed that the
 * contents of publication content lists do not change unless the corresponding preference
 * is disabled through the user interface.
 */
public abstract class PublicationContentList extends ContentList {

    private long id;

    @Override
    public String getPositionId() {
        return String.format(Locale.ENGLISH, "%064d", id);
    }

    void setPositionId(long id) {
        this.id = id;
    }
}
