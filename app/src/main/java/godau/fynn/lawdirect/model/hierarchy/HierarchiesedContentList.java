package godau.fynn.lawdirect.model.hierarchy;

import godau.fynn.lawdirect.model.content.ContentItem;
import godau.fynn.lawdirect.model.parent.ContentList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Content list that uses {@link Heading} classes to set hierarchy position
 * for {@link HierarchisedContentItem} classes
 */
public abstract class HierarchiesedContentList extends ContentList {

    @Override
    public final List<HierarchisedContentItem> getContent(int page) throws IOException {
        // This cast works because getContentItems(int) enforces that a List<HierarchisedContentItem> is returned
        List<HierarchisedContentItem> list = (List<HierarchisedContentItem>) super.getContent(page);

        int[] position = new int[0];
        Stack<String> breadcrumbs = new Stack<>();

        if (list != null) for (HierarchisedContentItem item : list) {
            if (item instanceof Heading) {
                position = item.getPositionInHierarchy();

                // Update breadcumbs
                // Remove items that are too much
                while (breadcrumbs.size() > position.length) {
                    breadcrumbs.pop();
                }

                // Set topmost breadcrumb
                if (position.length > breadcrumbs.size()) {
                    breadcrumbs.push(((Heading) item).getLabel());
                } else {
                    breadcrumbs.set(position.length - 1, ((Heading) item).getLabel());
                }
            }

            item.setPositionInHierarchy(position, breadcrumbs.toArray(new String[position.length]));
        }

        return list;
    }

    @Override
    protected abstract List<HierarchisedContentItem> getContentItems(int page) throws IOException;
}
