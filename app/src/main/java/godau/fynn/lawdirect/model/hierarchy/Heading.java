package godau.fynn.lawdirect.model.hierarchy;

import android.text.Spanned;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.util.Hierarchy;

public class Heading extends HierarchisedContentItem {

    private final String label;

    /**
     * @param position Path to this heading
     * @param label    Label of this heading, e.g. "Chapter 1", "Book 2", "Title 3"
     * @param title    Title of this heading to be displayed next to its label
     */
    public Heading(int[] position, String label, @Nullable String title) {
        super(title);
        this.label = label;

        setPositionInHierarchy(position, null);
    }

    public String getTitle() {
        if (title == null)
            return label;
        else
            return label + " – " + title;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public Spanned getText() {
        return null;
    }

    @Override
    public String getId() {
        return Hierarchy.printNumericalHierarchy(getPositionInHierarchy());
    }
}
