package godau.fynn.lawdirect.model.parent;

import android.text.Spanned;
import android.text.SpannedString;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.model.hierarchy.Book;

import java.io.IOException;
import java.util.List;

/**
 * A <code>LawProvider</code> implementation is the root item of a series of classes that provides
 * content from a specific law source.
 *
 * <p>Each provider should have its own pacakge in <code>godau.fynn.lawdirect.providers</code>.</p>
 */
public abstract class LawProvider extends SinglePageParentItem {

    /**
     * @return A list typically consisting of {@link Book}s and {@link Publication}s
     */
    public abstract List<ParentItem> getLawItems() throws IOException;

    public List<? extends ListItem> getContent() throws IOException {
        return getLawItems();
    }

    @Nullable
    @Override
    public Spanned getText() {
        if (getDataSourceName() == null) {
            return null;
        } else {
            return new SpannedString(getDataSourceName());
        }
    }

    @Nullable
    protected abstract String getDataSourceName();

    @Nullable
    public String getFavicon() {
        return null;
    }

}
