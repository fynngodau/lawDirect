package godau.fynn.lawdirect.model.hierarchy;

public abstract class NumberedItem extends HierarchisedContentItem {

    protected final String number;

    public NumberedItem(String number, String title) {
        super(title);
        this.number = number;
    }

    @Override
    public String getTitle() {
        if (title == null)
            return number;
        else
            return number + ' ' + title;
    }

}
