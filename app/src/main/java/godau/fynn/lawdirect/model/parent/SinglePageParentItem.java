package godau.fynn.lawdirect.model.parent;

import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;

import java.io.IOException;
import java.util.List;

/**
 * A {@link ParentItem} that is not paginated, thus only has one page, thus only has
 * content on page 0. This class is for convenience.
 */
public abstract class SinglePageParentItem extends BaseParentItem implements ParentItem {

    @Nullable
    @Override
    public List<? extends ListItem> getContent(int page) throws IOException {
        if (page == 0) {
            return getContent();
        } else {
            return null;
        }
    }

    public abstract @Nullable
    List<? extends ListItem> getContent() throws IOException;
}
