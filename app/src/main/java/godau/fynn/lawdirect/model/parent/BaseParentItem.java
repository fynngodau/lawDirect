package godau.fynn.lawdirect.model.parent;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.BaseItem;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.network.Network;

import java.io.IOException;
import java.util.List;

/**
 * Default implementation of {@link ParentItem}. It supports overwriting the display
 * content with the content of another parent item – this is used when loading items
 * from database while preserving their other specific method implementations.
 */
public abstract class BaseParentItem extends BaseItem implements ParentItem {

    private ParentItem overwrite;

    private transient Context context;
    private transient Network network;

    public void overwriteContent(ParentItem overwrite) {
        this.overwrite = overwrite;
    }

    public void setContext(@NonNull Context context) {
        this.context = context;
    }

    protected @NonNull
    Context getContext() {
        return context;
    }

    protected @NonNull
    Network getNetwork() {
        if (network == null) {
            network = new Network(context);
        }
        return network;
    }

    @Nullable
    @Override
    public final List<? extends ListItem> getDisplayContent(int page) throws IOException {
        if (overwrite == null) {
            return getContent(page);
        } else {
            overwrite.setContext(getContext());
            return overwrite.getDisplayContent(page);
        }
    }

    @Nullable
    protected abstract List<? extends ListItem> getContent(int page) throws IOException;

    public final ListItem getDisplayItem(String id) throws IOException {

        // If overwritten, invoke overwrite
        if (overwrite != null) {
            return overwrite.getDisplayItem(id);
        } else {
            return getItem(id);
        }
    }

    /**
     * Implement this method to attempt to find an item by its id. The default
     * implementation will iterate over all of your content pages. A more
     * performant implementation will increase performance with various tasks,
     * most notably bookmarks.
     *
     * <p>Note that in case an implementation fails to find such an item by
     * itself, but cannot guarantee that an item with this ID does not exist,
     * it should invoke this default method using <code>super.getItem(id)</code>.</p>
     *
     * @param id Id of the item to be found
     * @return The item with this id
     */
    protected @Nullable ListItem getItem(String id) throws IOException {
        // Find result by iterating over all pages

        int i;
        List<? extends ListItem> listItems = getDisplayContent(i = 0);

        do {
            for (ListItem listItem : listItems) {
                if (id.equals(listItem.getId())) {
                    return listItem;
                }
            }

            listItems = getDisplayContent(i++);
        } while (listItems != null);

        return null;
    }
}
