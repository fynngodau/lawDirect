package godau.fynn.lawdirect.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;

public class Network {

    private final Context context;

    @VisibleForTesting
    public static boolean assumeNetworkAvailable = false;

    public Network(Context context) {
        this.context = context;
    }

    /**
     * Make a GET request (synchronously)
     *
     * @param url           URL to be requested
     * @param body          Request body (JSON String)
     * @param requestMethod Usually either GET or POST
     * @return Response
     * @throws IOException If networking error or other IO exception
     */
    @NonNull
    public InputStream request(String url, @Nullable String body, String requestMethod) throws IOException {

        if (!isNetworkAvailable()) throw new IOException();

        URL connectwat = new URL(url);
        HttpsURLConnection urlConnection = (HttpsURLConnection) connectwat.openConnection();

        urlConnection.setRequestMethod(requestMethod);

        if (body != null) {

            OutputStream outputStream = urlConnection.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
            outputStreamWriter.write(body);
            outputStreamWriter.flush();
            outputStreamWriter.close();
            outputStream.close();
        }

        urlConnection.connect();

        return new BufferedInputStream(urlConnection.getInputStream());
    }

    protected boolean isNetworkAvailable() {

        if (assumeNetworkAvailable)
            return true;

        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Converts InputStream to String with UTF-8 charset
     */
    public static String string(InputStream in) throws IOException {
        return string(in, "UTF-8");
    }

    /**
     * Converts InputStream to String with provided charset
     */
    public static String string(InputStream in, String charsetName) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = in.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }

        in.close();
        String stringResult = result.toString(charsetName);
        result.close();

        return stringResult;

    }
}
