package godau.fynn.lawdirect.util.indicator;

import android.content.Context;
import godau.fynn.lawdirect.R;

public class NetworkErrorIndicatorItem extends LoadingIndicatorItem {

    private final Runnable retry;

    public NetworkErrorIndicatorItem(Context context, Runnable retry) {
        super(context);
        this.retry = retry;
    }

    @Override
    public String getTitle() {
        return context.getString(R.string.network_error);
    }

    public Runnable getRetryRunnable() {
        return retry;
    }
}
