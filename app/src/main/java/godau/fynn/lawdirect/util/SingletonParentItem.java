package godau.fynn.lawdirect.util;

import android.text.Spanned;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.parent.SinglePageParentItem;

import java.util.Collections;
import java.util.List;

public class SingletonParentItem<T extends ListItem> extends SinglePageParentItem {

    private final T content;
    private final String title, id;
    private final Spanned text;

    public SingletonParentItem(String title, @Nullable Spanned text, String id, T content) {
        this.content = content;
        this.title = title;
        this.text = text;
        this.id = id;
    }

    @Nullable
    @Override
    public List<T> getContent() {
        return Collections.singletonList(content);
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Nullable
    @Override
    public Spanned getText() {
        return text;
    }

    @Override
    public String getId() {
        return id;
    }
}
