package godau.fynn.lawdirect.util;

public class Hierarchy {

    public static String printNumericalHierarchy(int[] position) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < position.length; i++) {
            s.append(position[i]);
            if (i < position.length - 1) {
                s.append('-');
            }
        }
        return s.toString();
    }

    public static String printBreadcrumbs(String[] breadcrumbs) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < breadcrumbs.length; i++) {
            s.append(breadcrumbs[i]);
            if (i < breadcrumbs.length - 1) {
                s.append('-');
            }
        }
        return s.toString();
    }
}
