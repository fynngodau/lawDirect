package godau.fynn.lawdirect.util.indicator;

import android.content.Context;
import godau.fynn.lawdirect.R;

public class IncompatibleDataIndicatorItem extends LoadingIndicatorItem {
    public IncompatibleDataIndicatorItem(Context context) {
        super(context);
    }

    @Override
    public String getTitle() {
        return context.getString(R.string.incompatible_data_title);
    }
}
