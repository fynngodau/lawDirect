package godau.fynn.lawdirect.util.indicator;

import android.content.Context;
import android.text.Spanned;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.ListItem;

public class LoadingIndicatorItem implements ListItem {

    // Note: loading indicator items must not be serialized
    protected Context context;
    public LoadingIndicatorItem(Context context) {
        this.context = context;
    }

    @Override
    public String getTitle() {
        return context.getString(R.string.loading);
    }

    @Nullable
    @Override
    public Spanned getText() {
        return null;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public String getPositionId() {
        return null;
    }
}
