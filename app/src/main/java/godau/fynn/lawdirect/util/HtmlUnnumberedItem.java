package godau.fynn.lawdirect.util;

import android.text.Html;
import android.text.Spanned;
import godau.fynn.lawdirect.model.content.UnnumberedItem;

public class HtmlUnnumberedItem extends UnnumberedItem {

    private final String html;

    public HtmlUnnumberedItem(String html) {
        super(null);
        this.html = html;
    }

    @Override
    public Spanned getText() {
        return Html.fromHtml(html);
    }
}
