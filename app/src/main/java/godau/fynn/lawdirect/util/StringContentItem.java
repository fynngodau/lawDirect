package godau.fynn.lawdirect.util;

import android.text.Spanned;
import android.text.SpannedString;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.content.ContentItem;
import godau.fynn.lawdirect.model.hierarchy.HierarchisedContentItem;

public class StringContentItem extends HierarchisedContentItem {

    private final String text;
    private final String number;

    public StringContentItem(String number, @Nullable String title, String text) {
        super(title);
        this.text = text;
        this.number = number;
    }

    @Override
    public Spanned getText() {
        return new SpannedString(text);
    }

    @Override
    public String getId() {
        return number;
    }
}
