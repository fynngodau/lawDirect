package godau.fynn.lawdirect.util;

import android.text.Spanned;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.BaseItem;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.model.Searchable;
import godau.fynn.lawdirect.model.parent.BaseParentItem;

import java.io.IOException;
import java.util.List;

public class SearchResults extends BaseParentItem implements ParentItem {

    private final Searchable searchable;
    private final String query;

    public SearchResults(Searchable searchable, String query) {
        this.searchable = searchable;
        this.query = query;
    }

    @Override
    public String getTitle() {
        return getContext().getString(R.string.search_query, query, searchable.getTitle());
    }

    @Nullable
    @Override
    public Spanned getText() {
        return null;
    }

    @Override
    public String getId() {
        return "";
    }

    @Nullable
    @Override
    public List<? extends ListItem> getContent(int page) throws IOException {
        return searchable.search(query, page);
    }

    @Nullable
    @Override
    public ListItem getItem(String id) {
        throw new UnsupportedOperationException("Search results cannot query item by id");
    }
}
