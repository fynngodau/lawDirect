package godau.fynn.lawdirect.util;

import androidx.annotation.Nullable;
import androidx.core.util.Consumer;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.providers.LawProviderProvider;

import java.io.IOException;
import java.util.List;

public class Resolver {

    /**
     * Resolves the path to the item with this id and returns it
     *
     * @param pathId path starting with and at <code>/</code> to an ID
     * @return The item with this ID in this path
     */
    public static @Nullable
    ListItem resolve(String pathId) throws IOException {
        return resolve(pathId, null);
    }

    /**
     * Resolves the path to the item with this id and returns it while providing
     * each parent item that is on its path to the consumer
     *
     * @param pathId   path starting with and at <code>/</code> to an ID
     * @param consumer Consumer which receives each parent item on the path to
     *                 and including the returned item, but not the first item
     * @return The item with this ID in this path
     */
    public static @Nullable
    ListItem resolve(String pathId, @Nullable Consumer<ParentItem> consumer) throws IOException {
        String[] parts = pathId.split("/");

        ListItem item = new LawProviderProvider();

        for (String part : parts) {

            if (part.isEmpty()) {
                continue;
            }

            if (item == null) {
                return null;
            }

            item = ((ParentItem) item).getDisplayItem(part);

            if (consumer != null) {
                consumer.accept((ParentItem) item);
            }
        }

        return item;
    }

    /**
     * Utility method for finding the item with the provided id from
     * the provided selection
     */
    public static @Nullable
    ListItem findInList(List<? extends ListItem> list, String id) {
        if (list == null) {
            return null;
        }

        for (ListItem item : list) {
            if (id.equals(item.getId())) {
                return item;
            }
        }
        return null;
    }
}
